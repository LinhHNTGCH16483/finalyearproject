package com.example.eschool.config.datetime;

import com.example.eschool.common.DataUtils;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

    @Override
    public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException {
        String str = jsonParser.getText();
        try {
            return DataUtils.stringToLocalDateTime(str, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        } catch (DateTimeParseException e) {
            return deserialize2(str);
        }
    }

    private LocalDateTime deserialize2(String str) {
        try {
            return DataUtils.stringToLocalDateTime(str, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        } catch (DateTimeParseException e) {
            return deserialize3(str);
        }
    }

    private LocalDateTime deserialize3(String str) {
        try {
            return DataUtils.stringToLocalDateTime(str, DateTimeFormatter.ISO_DATE_TIME);
        } catch (DateTimeParseException e) {
            return deserialize4(str);
        }
    }

    private LocalDateTime deserialize4(String str) {
        try {
            return DataUtils.stringToLocalDateTime(str, DateTimeFormatter.ISO_INSTANT);
        } catch (DateTimeParseException e) {
            return deserialize5(str);
        }
    }

    private LocalDateTime deserialize5(String str) {
        return DataUtils.stringToLocalDateTime(str, DateTimeFormatter.RFC_1123_DATE_TIME);
    }
}
