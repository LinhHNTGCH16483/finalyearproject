package com.example.eschool.config.datetime;

import com.example.eschool.common.DataUtils;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateTimeSerializer extends JsonSerializer<LocalDateTime> {

    @Override
    public void serialize(LocalDateTime localDateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        String str = DataUtils.localDateTimeToString(localDateTime, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        jsonGenerator.writeString(str);
    }
}
