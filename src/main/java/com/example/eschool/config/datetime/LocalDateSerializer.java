package com.example.eschool.config.datetime;

import com.example.eschool.common.DataUtils;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class LocalDateSerializer extends JsonSerializer<LocalDate> {

    @Override
    public void serialize(LocalDate localDate, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        String str = DataUtils.localDateToString(localDate, DateTimeFormatter.ISO_LOCAL_DATE);
        jsonGenerator.writeString(str);
    }
}
