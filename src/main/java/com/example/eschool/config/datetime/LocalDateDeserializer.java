package com.example.eschool.config.datetime;

import com.example.eschool.common.DataUtils;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class LocalDateDeserializer extends JsonDeserializer<LocalDate> {

    @Override
    public LocalDate deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException {
        String str = jsonParser.getText();
        try {
            return DataUtils.stringToLocalDate(str, DateTimeFormatter.ISO_LOCAL_DATE);
        } catch (DateTimeParseException e) {
            return deserialize2(str);
        }
    }

    private LocalDate deserialize2(String str) {
        try {
            return DataUtils.stringToLocalDate(str, DateTimeFormatter.ISO_OFFSET_DATE);
        } catch (DateTimeParseException e) {
            return deserialize3(str);
        }
    }

    private LocalDate deserialize3(String str) {
        try {
            return DataUtils.stringToLocalDate(str, DateTimeFormatter.ISO_DATE);
        } catch (DateTimeParseException e) {
            return deserialize4(str);
        }
    }

    private LocalDate deserialize4(String str) {
        try {
            return DataUtils.stringToLocalDate(str, DateTimeFormatter.ISO_ORDINAL_DATE);
        } catch (DateTimeParseException e) {
            return deserialize5(str);
        }
    }

    private LocalDate deserialize5(String str) {
        try {
            return DataUtils.stringToLocalDate(str, DateTimeFormatter.ISO_WEEK_DATE);
        } catch (DateTimeParseException e) {
            return deserialize6(str);
        }
    }

    private LocalDate deserialize6(String str) {
        return DataUtils.stringToLocalDate(str, DateTimeFormatter.BASIC_ISO_DATE);
    }
}
