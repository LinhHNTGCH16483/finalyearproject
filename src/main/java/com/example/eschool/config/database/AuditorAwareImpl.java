package com.example.eschool.config.database;

import com.example.eschool.common.Constant.ThreadContextKey;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

import static com.example.eschool.common.DataUtils.notNullOrEmpty;

public class AuditorAwareImpl implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        String auditor = ThreadContext.get(ThreadContextKey.AUDITOR);
        return notNullOrEmpty(auditor) ? Optional.of(auditor) : Optional.of("LinhHNT");
    }
}
