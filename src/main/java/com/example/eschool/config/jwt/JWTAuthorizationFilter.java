package com.example.eschool.config.jwt;

import com.example.eschool.common.Constant.ClaimsKey;
import com.example.eschool.common.Constant.JwtInfo;
import com.example.eschool.common.Constant.ThreadContextKey;
import com.example.eschool.service.JwtService;
import com.example.eschool.service.impl.UserDetailServiceImpl;
import io.jsonwebtoken.Claims;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.example.eschool.common.DataUtils.isNull;
import static com.example.eschool.common.DataUtils.notNullOrEmpty;

@Component
public class JWTAuthorizationFilter extends OncePerRequestFilter {

    @Autowired
    private JwtService jwtService;

    @Autowired
    private UserDetailServiceImpl userDetailService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String header = httpServletRequest.getHeader(JwtInfo.HEADER);
        String token = null;
        String username = null;
        String role = null;
        String isActive = null;

        if (notNullOrEmpty(header) && header.startsWith(JwtInfo.PREFIX)) {
            token = header.replace(JwtInfo.PREFIX, "");

            Claims claims = jwtService.getClaimsFromToken(token);
            username = claims.get(ClaimsKey.USERNAME, String.class);
            role = claims.get(ClaimsKey.ROLE, String.class);
            isActive = String.valueOf(claims.get(ClaimsKey.IS_ACTIVE, Boolean.class));
        }
        if (notNullOrEmpty(username) && isNull(SecurityContextHolder.getContext().getAuthentication())) {
            UserDetails userDetails = userDetailService.loadUserByUsername(username);
            if (Boolean.TRUE.equals(jwtService.validateToken(token, userDetails))) {
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                ThreadContext.put(ThreadContextKey.AUDITOR, username);
                ThreadContext.put(ThreadContextKey.USERNAME, username);
                ThreadContext.put(ThreadContextKey.ROLE, role);
                ThreadContext.put(ThreadContextKey.IS_ACTIVE, isActive);
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
