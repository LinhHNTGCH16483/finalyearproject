package com.example.eschool.controller;

import com.example.eschool.api.ChatAPI;
import com.example.eschool.model.dto.MessageDTO;
import com.example.eschool.service.GroupChatHistoryService;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ChatController extends BaseController<GroupChatHistoryService> implements ChatAPI {

    public ChatController(GroupChatHistoryService groupChatHistoryService) {
        super(groupChatHistoryService);
    }

    @Override
    public void sendMessage(Long groupChatId, MessageDTO messageDTO) {
        service.sendMessage(groupChatId, messageDTO);
    }
}
