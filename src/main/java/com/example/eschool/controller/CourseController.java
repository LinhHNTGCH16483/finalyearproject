package com.example.eschool.controller;

import com.example.eschool.api.CourseAPI;
import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Course;
import com.example.eschool.response.GeneralResponse;
import com.example.eschool.response.ResponseCustom;
import com.example.eschool.service.CourseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CourseController extends BaseController<CourseService> implements CourseAPI {

    public CourseController(CourseService service) {
        super(service);
    }

    @Override
    public ResponseEntity<ResponseCustom<CountObjectResponseDTO>> countCourse() {
        return GeneralResponse.success(service.countCourse());
    }

    @Override
    public ResponseEntity<ResponseCustom<List<Course>>> getAllCourse() {
        return GeneralResponse.success(service.getAll());
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> addCourse(Course course) {
        service.save(course);
        return GeneralResponse.success(true);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> updateCourse(Long courseId, Course course) {
        Course entity = service.getById(courseId);

        course.setId(entity.getId());
        service.update(course);
        return GeneralResponse.success(true);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> deleteCourse(Long courseId) {
        service.delete(courseId);
        return GeneralResponse.success(true);
    }
}
