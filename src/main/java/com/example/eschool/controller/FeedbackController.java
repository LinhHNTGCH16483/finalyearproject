package com.example.eschool.controller;

import com.example.eschool.api.FeedbackAPI;
import com.example.eschool.model.entity.Feedback;
import com.example.eschool.response.GeneralResponse;
import com.example.eschool.response.ResponseCustom;
import com.example.eschool.service.FeedbackService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FeedbackController extends BaseController<FeedbackService> implements FeedbackAPI {

    public FeedbackController(FeedbackService service) {
        super(service);
    }

    @Override
    public ResponseEntity<ResponseCustom<List<Feedback>>> getAllFeedback() {
        return GeneralResponse.success(service.getAll());
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> addFeedback(Feedback feedback) {
        service.save(feedback);
        return GeneralResponse.success(true);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> updateFeedback(Long feedbackId, Feedback feedback) {
        Feedback entity = service.getById(feedbackId);

        feedback.setId(entity.getId());
        service.update(feedback);
        return GeneralResponse.success(true);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> deleteFeedback(Long feedbackId) {
        service.delete(feedbackId);
        return GeneralResponse.success(true);
    }
}
