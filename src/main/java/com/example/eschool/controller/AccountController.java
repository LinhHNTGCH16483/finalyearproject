package com.example.eschool.controller;

import com.example.eschool.api.AccountAPI;
import com.example.eschool.model.dto.LoginDTO;
import com.example.eschool.response.GeneralResponse;
import com.example.eschool.response.ResponseCustom;
import com.example.eschool.service.AccountService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController extends BaseController<AccountService> implements AccountAPI {

    public AccountController(AccountService service) {
        super(service);
    }

    @Override
    public ResponseEntity<ResponseCustom<LoginDTO>> login(LoginDTO loginDTO) {
        return GeneralResponse.success(service.login(loginDTO));
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> logout() {
        return GeneralResponse.success(service.logout());
    }
}
