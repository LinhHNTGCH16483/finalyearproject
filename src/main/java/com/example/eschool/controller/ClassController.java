package com.example.eschool.controller;

import com.example.eschool.api.ClassAPI;
import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Class;
import com.example.eschool.response.GeneralResponse;
import com.example.eschool.response.ResponseCustom;
import com.example.eschool.service.ClassService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ClassController extends BaseController<ClassService> implements ClassAPI {

    public ClassController(ClassService service) {
        super(service);
    }

    @Override
    public ResponseEntity<ResponseCustom<CountObjectResponseDTO>> countClass() {
        return GeneralResponse.success(service.countClass());
    }

    @Override
    public ResponseEntity<ResponseCustom<List<Class>>> getAllClass() {
        return GeneralResponse.success(service.getAll());
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> addClass(Class clazz) {
        service.save(clazz);
        return GeneralResponse.success(true);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> updateClass(Long classId, Class clazz) {
        Class entity = service.getById(classId);

        clazz.setId(entity.getId());
        service.update(clazz);
        return GeneralResponse.success(true);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> deleteClass(Long classId) {
        service.delete(classId);
        return GeneralResponse.success(true);
    }
}
