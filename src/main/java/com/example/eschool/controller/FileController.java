package com.example.eschool.controller;

import com.example.eschool.api.FileAPI;
import com.example.eschool.model.dto.FileDTO;
import com.example.eschool.response.GeneralResponse;
import com.example.eschool.response.ResponseCustom;
import com.example.eschool.service.FileService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class FileController extends BaseController<FileService> implements FileAPI {

    public FileController(FileService service) {
        super(service);
    }

    @Override
    public ResponseEntity<ResponseCustom<FileDTO>> uploadFile(MultipartFile file) throws IOException {
        return GeneralResponse.success(service.uploadFile(file));
    }

    @Override
    public ResponseEntity<byte[]> downloadFile(String bucketName, String filename) throws IOException {

        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment");
        headers.set(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS, "GET");
        return ResponseEntity
                .ok()
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .headers(headers)
                .body(service.downloadFile(bucketName, filename));
    }
}
