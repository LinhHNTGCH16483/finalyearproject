package com.example.eschool.controller;

import com.example.eschool.api.ScheduleAPI;
import com.example.eschool.model.dto.CalendarDTO;
import com.example.eschool.model.dto.ScheduleDTO;
import com.example.eschool.response.GeneralResponse;
import com.example.eschool.response.ResponseCustom;
import com.example.eschool.service.ScheduleService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
public class ScheduleController extends BaseController<ScheduleService> implements ScheduleAPI {

    public ScheduleController(ScheduleService service) {
        super(service);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> addSchedule(ScheduleDTO scheduleDTO) {
        return GeneralResponse.success(service.addSchedule(scheduleDTO));
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> updateSchedule(Long scheduleId, ScheduleDTO scheduleDTO) {
        return GeneralResponse.success(service.updateSchedule(scheduleId, scheduleDTO));
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> deleteSchedule(Long scheduleId) {
        return GeneralResponse.success(service.deleteSchedule(scheduleId));
    }

    @Override
    public ResponseEntity<ResponseCustom<List<CalendarDTO>>> getClassSchedule(Long classId, LocalDate fromDate, LocalDate toDate) {
        return GeneralResponse.success(service.getClassSchedule(classId, fromDate, toDate));
    }

    @Override
    public ResponseEntity<ResponseCustom<List<CalendarDTO>>> getStudentSchedule(Long studentId, LocalDate fromDate, LocalDate toDate) {
        return GeneralResponse.success(service.getStudentSchedule(studentId, fromDate, toDate));
    }

    @Override
    public ResponseEntity<ResponseCustom<List<CalendarDTO>>> getLecturerSchedule(Long lecturerId, LocalDate fromDate, LocalDate toDate) {
        return GeneralResponse.success(service.getLecturerSchedule(lecturerId, fromDate, toDate));
    }
}
