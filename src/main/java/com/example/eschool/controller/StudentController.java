package com.example.eschool.controller;

import com.example.eschool.api.StudentAPI;
import com.example.eschool.common.Paging;
import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.dto.SearchStudentResponseDTO;
import com.example.eschool.model.dto.StudentDTO;
import com.example.eschool.response.GeneralResponse;
import com.example.eschool.response.ResponseCustom;
import com.example.eschool.service.StudentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController extends BaseController<StudentService> implements StudentAPI {

    public StudentController(StudentService service) {
        super(service);
    }

    @Override
    public ResponseEntity<ResponseCustom<List<StudentDTO>>> searchStudent(String studentCode, String studentName, Long pageSize, Long pageNumber) {

        SearchStudentResponseDTO dto = service.searchStudent(studentCode, studentName, pageSize, pageNumber);

        return GeneralResponse.success(dto.getStudentDTOList(), Paging.setPaging(dto.getTotalRecords(), pageSize, pageNumber));
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> addStudent(StudentDTO studentDTO) {
        return GeneralResponse.success(service.addStudent(studentDTO));
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> updateStudent(Long studentId, StudentDTO studentDTO) {
        return GeneralResponse.success(service.updateStudent(studentId, studentDTO));
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> deleteStudent(Long studentId) {
        return GeneralResponse.success(service.deleteStudent(studentId));
    }

    @Override
    public ResponseEntity<ResponseCustom<CountObjectResponseDTO>> countStudent() {
        return GeneralResponse.success(service.countStudent());
    }
}
