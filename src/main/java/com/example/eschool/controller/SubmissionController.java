package com.example.eschool.controller;

import com.example.eschool.api.SubmissionAPI;
import com.example.eschool.model.entity.Submission;
import com.example.eschool.response.GeneralResponse;
import com.example.eschool.response.ResponseCustom;
import com.example.eschool.service.SubmissionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SubmissionController extends BaseController<SubmissionService> implements SubmissionAPI {

    public SubmissionController(SubmissionService service) {
        super(service);
    }

    @Override
    public ResponseEntity<ResponseCustom<List<Submission>>> getAllSubmission() {
        return GeneralResponse.success(service.getAll());
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> addSubmission(Submission submission) {
        service.save(submission);
        return GeneralResponse.success(true);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> updateSubmission(Long submissionId, Submission submission) {
        Submission entity = service.getById(submissionId);

        submission.setId(entity.getId());
        service.update(submission);
        return GeneralResponse.success(true);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> deleteSubmission(Long submissionId) {
        service.delete(submissionId);
        return GeneralResponse.success(true);
    }
}
