package com.example.eschool.controller;

import com.example.eschool.api.BlogAPI;
import com.example.eschool.model.entity.Blog;
import com.example.eschool.response.GeneralResponse;
import com.example.eschool.response.ResponseCustom;
import com.example.eschool.service.BlogService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BlogController extends BaseController<BlogService> implements BlogAPI {

    public BlogController(BlogService service) {
        super(service);
    }

    @Override
    public ResponseEntity<ResponseCustom<List<Blog>>> getAllBlog() {
        return GeneralResponse.success(service.getAll());
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> addBlog(Blog blog) {
        service.save(blog);
        return GeneralResponse.success(true);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> updateBlog(Long blogId, Blog blog) {
        Blog entity = service.getById(blogId);

        blog.setId(entity.getId());
        service.update(blog);
        return GeneralResponse.success(true);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> deleteBlog(Long blogId) {
        service.delete(blogId);
        return GeneralResponse.success(true);
    }
}
