package com.example.eschool.controller;

import com.example.eschool.api.LecturerAPI;
import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Lecturer;
import com.example.eschool.response.GeneralResponse;
import com.example.eschool.response.ResponseCustom;
import com.example.eschool.service.LecturerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LecturerController extends BaseController<LecturerService> implements LecturerAPI {

    public LecturerController(LecturerService service) {
        super(service);
    }

    @Override
    public ResponseEntity<ResponseCustom<CountObjectResponseDTO>> countLecturer() {
        return GeneralResponse.success(service.countLecturer());
    }

    @Override
    public ResponseEntity<ResponseCustom<List<Lecturer>>> getAllLecturer() {
        return GeneralResponse.success(service.getAll());
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> addLecturer(Lecturer lecturer) {
        service.save(lecturer);
        return GeneralResponse.success(true);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> updateLecturer(Long lecturerId, Lecturer lecturer) {
        Lecturer entity = service.getById(lecturerId);

        lecturer.setId(entity.getId());
        service.update(lecturer);
        return GeneralResponse.success(true);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> deleteLecturer(Long lecturerId) {
        service.delete(lecturerId);
        return GeneralResponse.success(true);
    }
}
