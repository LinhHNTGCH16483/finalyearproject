package com.example.eschool.controller;

public class BaseController<Service> {
    protected final Service service;

    public BaseController(Service service) {
        this.service = service;
    }
}
