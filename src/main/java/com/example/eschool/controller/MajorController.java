package com.example.eschool.controller;

import com.example.eschool.api.MajorAPI;
import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Major;
import com.example.eschool.response.GeneralResponse;
import com.example.eschool.response.ResponseCustom;
import com.example.eschool.service.MajorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MajorController extends BaseController<MajorService> implements MajorAPI {

    public MajorController(MajorService service) {
        super(service);
    }

    @Override
    public ResponseEntity<ResponseCustom<CountObjectResponseDTO>> countMajor() {
        return GeneralResponse.success(service.countMajor());
    }

    @Override
    public ResponseEntity<ResponseCustom<List<Major>>> getAllMajor() {
        return GeneralResponse.success(service.getAll());
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> addMajor(Major major) {
        service.save(major);
        return GeneralResponse.success(true);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> updateMajor(Long majorId, Major major) {
        Major entity = service.getById(majorId);

        major.setId(entity.getId());
        service.update(major);
        return GeneralResponse.success(true);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> deleteMajor(Long majorId) {
        service.delete(majorId);
        return GeneralResponse.success(true);
    }
}
