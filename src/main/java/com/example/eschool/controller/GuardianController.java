package com.example.eschool.controller;

import com.example.eschool.api.GuardianAPI;
import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Guardian;
import com.example.eschool.response.GeneralResponse;
import com.example.eschool.response.ResponseCustom;
import com.example.eschool.service.GuardianService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GuardianController extends BaseController<GuardianService> implements GuardianAPI {

    public GuardianController(GuardianService service) {
        super(service);
    }

    @Override
    public ResponseEntity<ResponseCustom<CountObjectResponseDTO>> countGuardian() {
        return GeneralResponse.success(service.countGuardian());
    }

    @Override
    public ResponseEntity<ResponseCustom<List<Guardian>>> getAllGuardian() {
        return GeneralResponse.success(service.getAll());
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> addGuardian(Guardian guardian) {
        service.save(guardian);
        return GeneralResponse.success(true);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> updateGuardian(Long guardianId, Guardian guardian) {
        Guardian entity = service.getById(guardianId);

        guardian.setId(entity.getId());
        service.update(guardian);
        return GeneralResponse.success(true);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> deleteGuardian(Long guardianId) {
        service.delete(guardianId);
        return GeneralResponse.success(true);
    }
}
