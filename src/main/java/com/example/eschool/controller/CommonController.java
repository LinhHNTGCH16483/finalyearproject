package com.example.eschool.controller;

import com.example.eschool.api.CommonAPI;
import com.example.eschool.model.dto.MasterDataDTO;
import com.example.eschool.response.GeneralResponse;
import com.example.eschool.response.ResponseCustom;
import com.example.eschool.service.MasterDataService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CommonController extends BaseController<MasterDataService> implements CommonAPI {

    public CommonController(MasterDataService masterDataService) {
        super(masterDataService);
    }

    @Override
    public ResponseEntity<ResponseCustom<List<MasterDataDTO>>> getMasterData(String lang) {
        return GeneralResponse.success(service.getMasterData(lang));
    }
}
