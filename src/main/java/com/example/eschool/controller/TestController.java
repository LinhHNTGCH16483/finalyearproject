package com.example.eschool.controller;

import com.example.eschool.api.TestAPI;
import com.example.eschool.model.dto.FileDTO;
import com.example.eschool.model.dto.LoginDTO;
import com.example.eschool.model.redis.Test;
import com.example.eschool.repository.redis.TestRepository;
import com.example.eschool.response.GeneralResponse;
import com.example.eschool.response.ResponseCustom;
import com.example.eschool.service.JwtService;
import com.example.eschool.service.MinioService;
import com.example.eschool.service.TestService;
import com.example.eschool.service.impl.UserDetailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class TestController extends BaseController<TestService> implements TestAPI {

    @Autowired
    private MinioService minioService;

    @Autowired
    private UserDetailServiceImpl userDetailService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private TestRepository testRepository;

    public TestController(TestService service) {
        super(service);
    }

    @Override
    public ResponseEntity<ResponseCustom<String>> login(LoginDTO loginDTO) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPassword()));
        UserDetails userDetails = userDetailService.loadUserByUsername(loginDTO.getUsername());
        return GeneralResponse.success(jwtService.generateToken(userDetails));
    }

    @Override
    public ResponseEntity<ResponseCustom<String>> testHelloWorld(String a) {
        return GeneralResponse.success(service.test(a));
//        return GeneralResponse.success(ThreadContext.get(Const.THREAD_CONTEXT_KEY.AUDITOR), String.class);
    }

    @Override
    public ResponseEntity<ResponseCustom<FileDTO>> uploadFile(MultipartFile file) throws IOException {
        return GeneralResponse.success(minioService.uploadFile(file, "linhhnt"));
    }

    @Override
    public ResponseEntity<byte[]> downloadFile(String filename) throws IOException {
        byte[] contents = minioService.downloadFile(filename, "linhhnt");

        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment");
        headers.set(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS, "GET");
        return ResponseEntity
                .ok()
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .headers(headers)
                .body(contents);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> deleteFile(String filename) {
        return GeneralResponse.success(minioService.deleteFile(filename, "Linhhnt"));
    }

    @Override
    public ResponseEntity<ResponseCustom<Test>> write() {
        Test test = Test.builder()
                .id("1")
                .a("1")
                .b("1")
                .c("1")
                .build();
        return GeneralResponse.success(testRepository.save(test));
    }

    @Override
    public ResponseEntity<ResponseCustom<Test>> read() {
        return GeneralResponse.success(testRepository.findById("1").orElse(null));
    }
}
