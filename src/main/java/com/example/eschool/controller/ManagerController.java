package com.example.eschool.controller;

import com.example.eschool.api.ManagerAPI;
import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Manager;
import com.example.eschool.response.GeneralResponse;
import com.example.eschool.response.ResponseCustom;
import com.example.eschool.service.ManagerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ManagerController extends BaseController<ManagerService> implements ManagerAPI {

    public ManagerController(ManagerService service) {
        super(service);
    }

    @Override
    public ResponseEntity<ResponseCustom<CountObjectResponseDTO>> countManager() {
        return GeneralResponse.success(service.countManager());
    }

    @Override
    public ResponseEntity<ResponseCustom<List<Manager>>> getAllManager() {
        return GeneralResponse.success(service.getAll());
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> addManager(Manager manager) {
        service.save(manager);
        return GeneralResponse.success(true);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> updateManager(Long managerId, Manager manager) {
        Manager entity = service.getById(managerId);

        manager.setId(entity.getId());
        service.update(manager);
        return GeneralResponse.success(true);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> deleteManager(Long managerId) {
        service.delete(managerId);
        return GeneralResponse.success(true);
    }
}
