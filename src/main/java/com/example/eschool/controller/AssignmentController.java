package com.example.eschool.controller;

import com.example.eschool.api.AssignmentAPI;
import com.example.eschool.model.entity.Assignment;
import com.example.eschool.response.GeneralResponse;
import com.example.eschool.response.ResponseCustom;
import com.example.eschool.service.AssignmentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AssignmentController extends BaseController<AssignmentService> implements AssignmentAPI {

    public AssignmentController(AssignmentService service) {
        super(service);
    }

    @Override
    public ResponseEntity<ResponseCustom<List<Assignment>>> getAllAssignment() {
        return GeneralResponse.success(service.getAll());
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> addAssignment(Assignment assignment) {
        service.save(assignment);
        return GeneralResponse.success(true);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> updateAssignment(Long assignmentId, Assignment assignment) {
        Assignment entity = service.getById(assignmentId);

        assignment.setId(entity.getId());
        service.update(assignment);
        return GeneralResponse.success(true);
    }

    @Override
    public ResponseEntity<ResponseCustom<Boolean>> deleteAssignment(Long assignmentId) {
        service.delete(assignmentId);
        return GeneralResponse.success(true);
    }
}
