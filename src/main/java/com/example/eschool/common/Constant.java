package com.example.eschool.common;

import java.util.Arrays;
import java.util.List;

public class Constant {

    public static final List<String> FILE_EXTENSION_LIST = Arrays.asList("txt", "docx", "pdf", "xlsx", "pptx", "png", "jpg", "rar", "zip");

    public static class ThreadContextKey {
        public static final String AUDITOR = "auditor";
        public static final String USERNAME = "username";
        public static final String ROLE = "role";
        public static final String IS_ACTIVE = "isActive";
    }

    public static class MessageCode {
        public static final String ERR_BUCKET_NAME_NOT_FOUND = "err.bucket.name.not.found";
        public static final String ERR_SLOT_IS_BOOKED = "err.slot.is.booked";
        public static final String ERR_BOOK_MORE_THAN_ONE_SLOT = "err.book.more.than.one.slot";
        public static final String ERR_INVALID_FILE_NAME = "err.invalid.file.name";
        public static final String ERR_FROM_DATE_TO_DATE_MORE_THAN_SEVEN_DAY = "err.from.date.to.date.more.than.seven.day";
        public static final String ERR_FROM_DATE_IS_NOT_MONDAY = "err.from.date.is.not.monday";
        public static final String ERR_TOKEN_NOT_FOUND = "err.token.not.found";
        public static final String ERR_FILE_NOT_FOUND = "err.file.not.found";
        public static final String ERR_SCHEDULE_NOT_FOUND = "err.schedule.not.found";
        public static final String ERR_CLASS_NOT_FOUND = "err.class.not.found";
        public static final String ERR_ACCOUNT_NOT_FOUND = "err.account.not.found";
        public static final String ERR_STUDENT_NOT_FOUND = "err.student.not.found";
        public static final String ERR_LECTURER_NOT_FOUND = "err.lecturer.not.found";
        public static final String ERR_WRONG_USERNAME_PASSWORD = "err.wrong.username.password";
        public static final String GROUP_CHAT_NOT_EXIST = "err.group.chat.not.found";
        public static final String GROUP_CHAT_IS_NOT_MEMBER = "err.group.chat.is.not.member";
    }

    public static class Gender {
        public static final String MALE = "Male";
        public static final String FEMALE = "Female";
        public static final String OTHER = "Other";
    }

    public static class Role {
        public static final String ADMIN = "ADMIN";
        public static final String MANAGER = "MANAGER";
        public static final String LECTURER = "LECTURER";
        public static final String STUDENT = "STUDENT";
        public static final String GUARDIAN = "GUARDIAN";
    }

    public static class ClaimsKey {
        public static final String USERNAME = "username";
        public static final String ROLE = "role";
        public static final String IS_ACTIVE = "isActive";
    }

    public static class JwtInfo {
        public static final long EXPIRATION_TIME = 86_400_000; //24h
        public static final String HEADER = "Authorization";
        public static final String PREFIX = "Bearer ";
        public static final String SECRET_KEY = "PhanHongNhung170298";
    }

    public static class EmailInfo {
        public static final String ACCOUNT = "ESchool.PleaseDoNotReply@gmail.com";
        public static final String PASSWORD = "!QAZxsw2#EDC";
        public static final String SUBJECT = "[E-School] Notification!!!";
    }

    public static class ColumnName {
        public static final String ID = "id";
        public static final String IS_DELETED = "is_deleted";
    }

    public static class IsDeleted {
        public static final Boolean TRUE = true;
        public static final Boolean FALSE = false;
    }

    public static class IsActive {
        public static final Boolean TRUE = true;
        public static final Boolean FALSE = false;
    }

    public enum ScheduleStatus{
        CREATE,
        UPDATE,
        DELETE;
    }
}
