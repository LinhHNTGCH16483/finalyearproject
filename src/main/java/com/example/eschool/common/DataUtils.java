package com.example.eschool.common;

import com.example.eschool.model.dto.CalendarDTO;
import com.example.eschool.model.dto.ScheduleDTO;
import com.example.eschool.model.dto.SlotDTO;
import org.apache.commons.lang3.StringEscapeUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Collection;

public class DataUtils {

    public static boolean isNull(Object object) {
        return object == null;
    }

    public static boolean notNull(Object object) {
        return !isNull(object);
    }

    public static boolean isEmpty(String str) {
        return str.isEmpty();
    }

    public static boolean isNullOrEmpty(String str) {
        return isNull(str) || isEmpty(str);
    }

    public static boolean isNullOrEmpty(Collection<?> collection) {
        return isNull(collection) || collection.isEmpty();
    }

    public static boolean notNullOrEmpty(String str) {
        return !isNullOrEmpty(str);
    }

    public static boolean notNullOrEmpty(Collection<?> collection) {
        return !isNullOrEmpty(collection);
    }

    public static String formatFilename(String filename) {
        return filename.replaceAll(" ", "_");
    }

    public static LocalDateTime stringToLocalDateTime(String str, DateTimeFormatter dateTimeFormatter) {
        if (isNullOrEmpty(str)) {
            return null;
        }
        return LocalDateTime.parse(str, dateTimeFormatter);
    }

    public static LocalDate stringToLocalDate(String str, DateTimeFormatter dateTimeFormatter) {
        if (isNullOrEmpty(str)) {
            return null;
        }
        return LocalDate.parse(str, dateTimeFormatter);
    }

    public static String localDateTimeToString(LocalDateTime value, DateTimeFormatter dateTimeFormatter) {
        if (isNull(value)) {
            return null;
        }
        return value.format(dateTimeFormatter);
    }

    public static String localDateToString(LocalDate value, DateTimeFormatter dateTimeFormatter) {
        if (isNull(value)) {
            return null;
        }
        return value.format(dateTimeFormatter);
    }

    public static Long parseToLong(Object obj) {
        if (isNull(obj)) {
            return null;
        }
        return Long.valueOf(String.valueOf(obj));
    }

    public static String parseToString(Object obj) {
        if (isNull(obj)) {
            return null;
        }
        return String.valueOf(obj);
    }

    public static Boolean parseToBoolean(Object obj) {
        if (isNull(obj)) {
            return false;
        }
        return Boolean.valueOf(String.valueOf(obj));
    }

    public static LocalDate parseToLocalDate(Object obj) {
        if (isNull(obj)) {
            return null;
        }
        return LocalDate.parse(String.valueOf(obj));
    }

    public static LocalDateTime parseToLocalDateTime(Object obj) {
        if (isNull(obj)) {
            return null;
        }
        return LocalDateTime.parse(String.valueOf(obj).replace(" ", "T"));
    }

    public static String generateStudentCode() {
        return "STU" + Calendar.getInstance().get(Calendar.YEAR) + System.nanoTime();
    }

    public static String generateGuardianCode() {
        return "GUA" + Calendar.getInstance().get(Calendar.YEAR) + System.nanoTime();
    }

    public static String generateLecturerCode() {
        return "LEC" + Calendar.getInstance().get(Calendar.YEAR) + System.nanoTime();
    }

    public static String generateManagerCode() {
        return "MAN" + Calendar.getInstance().get(Calendar.YEAR) + System.nanoTime();
    }

    public static String generateMajorCode() {
        return "MAJ" + System.nanoTime();
    }

    public static String generateCourseCode() {
        return "COU" + System.nanoTime();
    }

    public static String generateClassCode() {
        return "CLA" + System.nanoTime();
    }

    public static CalendarDTO mergeSchedule(CalendarDTO calendarDTO, ScheduleDTO scheduleDto) {
        return CalendarDTO.builder()
                .date(calendarDTO.getDate())
                .slot1(SlotDTO.builder()
                        .classId(mergeClassId(calendarDTO.getSlot1().getClassId(), scheduleDto.getClassId(), calendarDTO.getSlot1().getIsBooked(), scheduleDto.getSlot1()))
                        .isBooked(mergeSlot(calendarDTO.getSlot1().getIsBooked(), scheduleDto.getSlot1()))
                        .build())
                .slot2(SlotDTO.builder()
                        .classId(mergeClassId(calendarDTO.getSlot2().getClassId(), scheduleDto.getClassId(), calendarDTO.getSlot2().getIsBooked(), scheduleDto.getSlot2()))
                        .isBooked(mergeSlot(calendarDTO.getSlot2().getIsBooked(), scheduleDto.getSlot2()))
                        .build())
                .slot3(SlotDTO.builder()
                        .classId(mergeClassId(calendarDTO.getSlot3().getClassId(), scheduleDto.getClassId(), calendarDTO.getSlot3().getIsBooked(), scheduleDto.getSlot3()))
                        .isBooked(mergeSlot(calendarDTO.getSlot3().getIsBooked(), scheduleDto.getSlot3()))
                        .build())
                .slot4(SlotDTO.builder()
                        .classId(mergeClassId(calendarDTO.getSlot4().getClassId(), scheduleDto.getClassId(), calendarDTO.getSlot4().getIsBooked(), scheduleDto.getSlot4()))
                        .isBooked(mergeSlot(calendarDTO.getSlot4().getIsBooked(), scheduleDto.getSlot4()))
                        .build())
                .slot5(SlotDTO.builder()
                        .classId(mergeClassId(calendarDTO.getSlot5().getClassId(), scheduleDto.getClassId(), calendarDTO.getSlot5().getIsBooked(), scheduleDto.getSlot5()))
                        .isBooked(mergeSlot(calendarDTO.getSlot5().getIsBooked(), scheduleDto.getSlot5()))
                        .build())
                .slot6(SlotDTO.builder()
                        .classId(mergeClassId(calendarDTO.getSlot6().getClassId(), scheduleDto.getClassId(), calendarDTO.getSlot6().getIsBooked(), scheduleDto.getSlot6()))
                        .isBooked(mergeSlot(calendarDTO.getSlot6().getIsBooked(), scheduleDto.getSlot6()))
                        .build())
                .slot7(SlotDTO.builder()
                        .classId(mergeClassId(calendarDTO.getSlot7().getClassId(), scheduleDto.getClassId(), calendarDTO.getSlot7().getIsBooked(), scheduleDto.getSlot7()))
                        .isBooked(mergeSlot(calendarDTO.getSlot7().getIsBooked(), scheduleDto.getSlot7()))
                        .build())
                .slot8(SlotDTO.builder()
                        .classId(mergeClassId(calendarDTO.getSlot8().getClassId(), scheduleDto.getClassId(), calendarDTO.getSlot8().getIsBooked(), scheduleDto.getSlot8()))
                        .isBooked(mergeSlot(calendarDTO.getSlot8().getIsBooked(), scheduleDto.getSlot8()))
                        .build())
                .build();
    }

    private static Boolean mergeSlot(Boolean a, Boolean b) {
        return a || b;
    }

    private static Long mergeClassId(Long str1, Long str2, boolean slot1, boolean slot2) {
        if (slot1 || slot2) {
            return isNull(str2) ? str1 : str2;
        }
        return null;
    }

    public static void main(String[] args) {
        System.out.println(StringEscapeUtils.escapeJava("Khoảng thời gian tìm kiếm phải là bảy ngày"));
        System.out.println(StringEscapeUtils.escapeJava("Thời gian tìm kiếm phải bắt đầu từ thứ hai"));
    }
}
