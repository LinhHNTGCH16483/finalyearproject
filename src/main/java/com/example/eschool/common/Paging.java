package com.example.eschool.common;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Paging {

    private long totalElement;
    private long pageSize;
    private long totalPage;
    private long currentPage;

    public static Paging setPaging(long totalElement, long pageSize, long pageNumber) {
        if (pageSize == 0) {
            return null;
        }

        long totalPage = totalElement / pageSize + (totalElement % pageSize == 0 ? 0 : 1);

        return Paging.builder()
                .totalElement(totalElement)
                .pageSize(pageSize)
                .totalPage(totalPage)
                .currentPage(pageNumber)
                .build();
    }
}