package com.example.eschool.common;

import com.example.eschool.model.dto.*;
import com.example.eschool.model.entity.*;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Transformer {

    public static AccountDTO transformToAccountDto(Account account, boolean isGetAll) {
        AccountDTO accountDTO = AccountDTO.builder()
                .id(account.getId())
                .username(account.getUsername())
//                .password(account.getPassword())
                .role(account.getRole())
                .isActive(account.getIsActive())
                .lastLogin(account.getLastLogin())
                .firstName(account.getFirstName())
                .lastName(account.getLastName())
                .dateOfBirth(account.getDateOfBirth())
                .placeOfBirth(account.getPlaceOfBirth())
                .gender(account.getGender())
                .nationality(account.getNationality())
                .religion(account.getReligion())
                .maritalStatus(account.getMaritalStatus())
                .permanentAddress(account.getPermanentAddress())
                .phoneNumber(account.getPhoneNumber())
                .email(account.getEmail())
                .avatarImage(account.getAvatarImage())
                .build();
        if (isGetAll) {
            accountDTO.setIsDeleted(account.getIsDeleted());
            accountDTO.setCreatedBy(account.getCreatedBy());
            accountDTO.setCreatedDate(account.getCreatedDate());
            accountDTO.setLastModifiedBy(account.getLastModifiedBy());
            accountDTO.setLastModifiedDate(account.getLastModifiedDate());
        }
        return accountDTO;
    }

    public static MasterDataDTO transformToMasterDataDTO(MasterData masterData, boolean isGetAll) {
        MasterDataDTO masterDataDTO = MasterDataDTO.builder()
                .id(masterData.getId())
                .fieldId(masterData.getFieldId())
                .lang(masterData.getLang())
                .name(masterData.getName())
                .build();
        if (isGetAll) {
            masterDataDTO.setIsDeleted(masterData.getIsDeleted());
            masterDataDTO.setCreatedBy(masterData.getCreatedBy());
            masterDataDTO.setCreatedDate(masterData.getCreatedDate());
            masterDataDTO.setLastModifiedBy(masterData.getLastModifiedBy());
            masterDataDTO.setLastModifiedDate(masterData.getLastModifiedDate());
        }
        return masterDataDTO;
    }

    public static List<StudentDTO> transformToListStudentDTO(List<Object[]> objects) {
        List<StudentDTO> result = new ArrayList<>(objects.size());
        for (Object[] obj : objects) {
            int i = -1;
            result.add(StudentDTO.builder()
                    .id(DataUtils.parseToLong(obj[++i]))
                    .accountId(DataUtils.parseToLong(obj[++i]))
                    .code(DataUtils.parseToString(obj[++i]))
                    .admissionDate(DataUtils.parseToLocalDate(obj[++i]))
                    .isDeleted(DataUtils.parseToBoolean(obj[++i]))
                    .createdBy(DataUtils.parseToString(obj[++i]))
                    .createdDate(DataUtils.parseToLocalDateTime(obj[++i]))
                    .lastModifiedBy(DataUtils.parseToString(obj[++i]))
                    .lastModifiedDate(DataUtils.parseToLocalDateTime(obj[++i]))
                    .username(DataUtils.parseToString(obj[++i]))
                    .password(DataUtils.parseToString(obj[++i]))
                    .role(DataUtils.parseToString(obj[++i]))
                    .isAccountActive(DataUtils.parseToBoolean(obj[++i]))
                    .lastLogin(DataUtils.parseToLocalDateTime(obj[++i]))
                    .firstName(DataUtils.parseToString(obj[++i]))
                    .lastName(DataUtils.parseToString(obj[++i]))
                    .dateOfBirth(DataUtils.parseToLocalDate(obj[++i]))
                    .placeOfBirth(DataUtils.parseToString(obj[++i]))
                    .gender(DataUtils.parseToString(obj[++i]))
                    .nationality(DataUtils.parseToString(obj[++i]))
                    .religion(DataUtils.parseToString(obj[++i]))
                    .maritalStatus(DataUtils.parseToString(obj[++i]))
                    .permanentAddress(DataUtils.parseToString(obj[++i]))
                    .phoneNumber(DataUtils.parseToString(obj[++i]))
                    .email(DataUtils.parseToString(obj[++i]))
                    .avatarImage(DataUtils.parseToString(obj[++i]))
                    .isAccountDeleted(DataUtils.parseToBoolean(obj[++i]))
                    .accountCreatedBy(DataUtils.parseToString(obj[++i]))
                    .accountCreatedDate(DataUtils.parseToLocalDateTime(obj[++i]))
                    .accountLastModifiedBy(DataUtils.parseToString(obj[++i]))
                    .accountLastModifiedDate(DataUtils.parseToLocalDateTime(obj[++i]))
                    .build());
        }
        return result;
    }

    public static List<StudentDTO> transformToListStudentDTO(List<Student> studentList, List<Account> accountList) {

        Map<Long, Student> studentMap = studentList.stream().collect(Collectors.toMap(Student::getAccountId, Function.identity()));
        Map<Long, Account> accountMap = accountList.stream().collect(Collectors.toMap(Account::getId, Function.identity()));

        Set<Long> idSet = new HashSet<>();
        idSet.addAll(studentMap.keySet());
        idSet.addAll(accountMap.keySet());

        return idSet.stream().map(id -> {
            Student student = studentMap.get(id);
            Account account = accountMap.get(id);
            return StudentDTO.builder()
                    .id(student.getId())
                    .accountId(student.getAccountId())
                    .code(student.getCode())
                    .admissionDate(student.getAdmissionDate())
                    .isDeleted(student.getIsDeleted())
                    .createdBy(student.getCreatedBy())
                    .createdDate(student.getCreatedDate())
                    .lastModifiedBy(student.getLastModifiedBy())
                    .lastModifiedDate(student.getLastModifiedDate())
                    .username(account.getUsername())
                    .password(account.getPassword())
                    .role(account.getRole())
                    .isAccountActive(account.getIsActive())
                    .lastLogin(account.getLastLogin())
                    .firstName(account.getFirstName())
                    .lastName(account.getLastName())
                    .fullName(account.getFullName())
                    .dateOfBirth(account.getDateOfBirth())
                    .placeOfBirth(account.getPlaceOfBirth())
                    .gender(account.getGender())
                    .nationality(account.getNationality())
                    .religion(account.getReligion())
                    .maritalStatus(account.getMaritalStatus())
                    .permanentAddress(account.getPermanentAddress())
                    .phoneNumber(account.getPhoneNumber())
                    .email(account.getEmail())
                    .avatarImage(account.getAvatarImage())
                    .isAccountDeleted(account.getIsDeleted())
                    .accountCreatedBy(account.getCreatedBy())
                    .accountCreatedDate(account.getCreatedDate())
                    .accountLastModifiedBy(account.getLastModifiedBy())
                    .accountLastModifiedDate(account.getLastModifiedDate())
                    .build();
        }).collect(Collectors.toList());
    }

    public static List<ClassStudentDTO> transformToListClassStudentDTO(List<ClassStudent> classStudentList) {
        List<ClassStudentDTO> classStudentDTOList = new ArrayList<>(classStudentList.size());
        for (ClassStudent entity : classStudentList) {
            classStudentDTOList.add(ClassStudentDTO.builder()
                    .id(entity.getId())
                    .classId(entity.getClassId())
                    .studentId(entity.getStudentId())
                    .isDeleted(entity.getIsDeleted())
                    .createdBy(entity.getCreatedBy())
                    .createdDate(entity.getCreatedDate())
                    .lastModifiedBy(entity.getLastModifiedBy())
                    .lastModifiedDate(entity.getLastModifiedDate())
                    .build());
        }
        return classStudentDTOList;
    }

    public static List<ScheduleDTO> transformToListScheduleDTO(List<Schedule> scheduleList) {
        List<ScheduleDTO> scheduleDTOList = new ArrayList<>(scheduleList.size());
        for (Schedule entity : scheduleList) {
            scheduleDTOList.add(ScheduleDTO.builder()
                    .id(entity.getId())
                    .classId(entity.getClassId())
                    .date(entity.getDate())
                    .slot1(entity.getSlot1())
                    .slot2(entity.getSlot2())
                    .slot3(entity.getSlot3())
                    .slot4(entity.getSlot4())
                    .slot5(entity.getSlot5())
                    .slot6(entity.getSlot6())
                    .slot7(entity.getSlot7())
                    .slot8(entity.getSlot8())
                    .isDeleted(entity.getIsDeleted())
                    .createdBy(entity.getCreatedBy())
                    .createdDate(entity.getCreatedDate())
                    .lastModifiedBy(entity.getLastModifiedBy())
                    .lastModifiedDate(entity.getLastModifiedDate())
                    .build());
        }
        return scheduleDTOList;
    }
}
