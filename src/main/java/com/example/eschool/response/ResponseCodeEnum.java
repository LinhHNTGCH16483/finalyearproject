package com.example.eschool.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum ResponseCodeEnum {

    SUCCESS("200", 200),
    VALIDATION_ERROR("400", 400),
    UNAUTHORISED("401", 401),
    ACCESS_DENIED("403", 403),
    UNKNOWN_ERROR("500", 500);

    private String code;
    private int httpCode;
}
