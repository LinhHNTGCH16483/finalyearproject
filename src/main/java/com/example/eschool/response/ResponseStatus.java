package com.example.eschool.response;

import com.example.eschool.common.MessageUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseStatus {

    private String code;

    private String message;

    private String description;

    public ResponseStatus(String code) {
        this.code = code;
        this.message = MessageUtils.getMessage(code);
    }

    public ResponseStatus(String code, String description) {
        this.code = code;
        this.message = MessageUtils.getMessage(code);
        this.description = description;
    }
}
