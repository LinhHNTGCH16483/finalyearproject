package com.example.eschool.response;

import com.example.eschool.common.Paging;
import org.springframework.http.ResponseEntity;

public class GeneralResponse {

    public static <T> ResponseEntity<ResponseCustom<T>> fail(ResponseCodeEnum code, String message) {

        ResponseStatus responseStatus = new ResponseStatus(code.getCode(), message);
        ResponseCustom<T> responseCustom = new ResponseCustom<>();
        responseCustom.setStatus(responseStatus);
        return ResponseEntity.status(code.getHttpCode()).body(responseCustom);
    }

    public static <T> ResponseEntity<ResponseCustom<T>> success(T data) {

        ResponseStatus responseStatus = new ResponseStatus(ResponseCodeEnum.SUCCESS.getCode());
        ResponseCustom<T> responseCustom = new ResponseCustom<>();
        responseCustom.setStatus(responseStatus);
        responseCustom.setData(data);
        return ResponseEntity.ok(responseCustom);
    }

    public static <T> ResponseEntity<ResponseCustom<T>> success(T data, Paging paging) {
        ResponseStatus responseStatus = new ResponseStatus(ResponseCodeEnum.SUCCESS.getCode());
        ResponseCustom<T> responseCustom = new ResponseCustom<>();
        responseCustom.setStatus(responseStatus);
        responseCustom.setData(data);
        responseCustom.setPaging(paging);
        return ResponseEntity.ok(responseCustom);
    }
}
