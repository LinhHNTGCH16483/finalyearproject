package com.example.eschool.exception;

import com.example.eschool.response.ResponseCodeEnum;
import com.example.eschool.response.ResponseCustom;
import com.example.eschool.response.ResponseStatus;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

public class CustomAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {
        httpServletResponse.setContentType("application/json;charset=UTF-8");
        httpServletResponse.setStatus(ResponseCodeEnum.UNAUTHORISED.getHttpCode());

        ResponseCustom<Object> responseCustom = ResponseCustom
                .builder()
                .status(new ResponseStatus(ResponseCodeEnum.UNAUTHORISED.getCode()))
                .build();
        OutputStream out = httpServletResponse.getOutputStream();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(out, responseCustom);
        out.flush();
    }
}
