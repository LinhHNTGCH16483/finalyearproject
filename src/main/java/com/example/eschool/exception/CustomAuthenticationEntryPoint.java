package com.example.eschool.exception;

import com.example.eschool.response.ResponseCodeEnum;
import com.example.eschool.response.ResponseCustom;
import com.example.eschool.response.ResponseStatus;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        httpServletResponse.setContentType("application/json;charset=UTF-8");
        httpServletResponse.setStatus(ResponseCodeEnum.ACCESS_DENIED.getHttpCode());

        ResponseCustom<Object> responseCustom = ResponseCustom
                .builder()
                .status(new ResponseStatus(ResponseCodeEnum.ACCESS_DENIED.getCode()))
                .build();
        OutputStream out = httpServletResponse.getOutputStream();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(out, responseCustom);
        out.flush();
    }
}
