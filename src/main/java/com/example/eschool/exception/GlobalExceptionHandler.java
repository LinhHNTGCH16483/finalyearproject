package com.example.eschool.exception;

import com.example.eschool.response.GeneralResponse;
import com.example.eschool.response.ResponseCodeEnum;
import com.example.eschool.response.ResponseCustom;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ResponseCustom<String>> handleAllExceptions(Exception ex) {
        return GeneralResponse.fail(ResponseCodeEnum.UNKNOWN_ERROR, ex.getMessage());
    }

    @ExceptionHandler({IllegalArgumentException.class})
    public final ResponseEntity<ResponseCustom<String>> handleValidationExceptions(RuntimeException ex) {
        return GeneralResponse.fail(ResponseCodeEnum.VALIDATION_ERROR, ex.getMessage());
    }
}
