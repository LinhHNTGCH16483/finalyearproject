package com.example.eschool.service;

import com.example.eschool.model.dto.FileDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface MinioService {

    FileDTO uploadFile(MultipartFile file, String bucketName) throws IOException;

    byte[] downloadFile(String filename, String bucketName) throws IOException;

    Boolean deleteFile(String file, String bucketName);
}