package com.example.eschool.service;

import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Manager;

public interface ManagerService extends BaseService<Manager, Long> {

    CountObjectResponseDTO countManager();
}
