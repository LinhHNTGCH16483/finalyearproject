package com.example.eschool.service;

import com.example.eschool.model.entity.BlogComment;

public interface BlogCommentService extends BaseService<BlogComment, Long> {
}
