package com.example.eschool.service;

import com.example.eschool.model.entity.Submission;

public interface SubmissionService extends BaseService<Submission, Long> {
}
