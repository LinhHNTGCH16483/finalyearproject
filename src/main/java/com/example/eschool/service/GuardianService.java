package com.example.eschool.service;

import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Guardian;

public interface GuardianService extends BaseService<Guardian, Long> {

    CountObjectResponseDTO countGuardian();
}
