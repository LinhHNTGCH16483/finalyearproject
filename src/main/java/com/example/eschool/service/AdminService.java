package com.example.eschool.service;

import com.example.eschool.model.entity.Admin;

public interface AdminService extends BaseService<Admin, Long> {
}
