package com.example.eschool.service.impl;

import com.example.eschool.model.entity.GroupChat;
import com.example.eschool.repository.database.GroupChatRepository;
import com.example.eschool.service.GroupChatService;
import org.springframework.stereotype.Service;

@Service
public class GroupChatServiceImpl extends BaseServiceImpl<GroupChat, Long, GroupChatRepository> implements GroupChatService {

    public GroupChatServiceImpl(GroupChatRepository repository) {
        super(repository);
    }
}
