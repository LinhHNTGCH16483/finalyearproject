package com.example.eschool.service.impl;

import com.example.eschool.common.Constant;
import com.example.eschool.common.DataUtils;
import com.example.eschool.common.MessageUtils;
import com.example.eschool.common.Transformer;
import com.example.eschool.model.dto.AccountDTO;
import com.example.eschool.model.dto.LoginDTO;
import com.example.eschool.model.entity.Account;
import com.example.eschool.model.redis.TokenRedis;
import com.example.eschool.repository.database.AccountRepository;
import com.example.eschool.service.AccountService;
import com.example.eschool.service.JwtService;
import com.example.eschool.service.RedisService;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Optional;

@Service
public class AccountServiceImpl extends BaseServiceImpl<Account, Long, AccountRepository> implements AccountService {

    @Autowired
    private UserDetailServiceImpl userDetailService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private RedisService redisService;

    public AccountServiceImpl(AccountRepository repository) {
        super(repository);
    }

    @Override
    public LoginDTO login(LoginDTO loginDTO) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPassword()));
        UserDetails userDetails = userDetailService.loadUserByUsername(loginDTO.getUsername());

        String token = jwtService.generateToken(userDetails);

        TokenRedis tokenRedis = redisService.findTokenByUsername(loginDTO.getUsername());

        AccountDTO accountDTO = this.findByUserName(userDetails.getUsername());

        if (DataUtils.isNull(tokenRedis)) {
            tokenRedis = TokenRedis.builder()
                    .id(String.valueOf(accountDTO.getId()))
                    .username(userDetails.getUsername())
                    .token(token)
                    .build();
            redisService.addToken(tokenRedis);

            return LoginDTO.builder()
                    .username(loginDTO.getUsername())
                    .role(accountDTO.getRole())
                    .token(token)
                    .build();
        }

        redisService.updateToken(userDetails.getUsername(), token);
        return LoginDTO.builder()
                .username(loginDTO.getUsername())
                .role(accountDTO.getRole())
                .token(token)
                .build();
    }

    @Override
    public Boolean logout() {
        String currentUser = ThreadContext.get(Constant.ThreadContextKey.USERNAME);
        return redisService.deleteToken(currentUser);
    }

    @Override
    public AccountDTO findByUserName(String userName) {
        Account account = repository.findByUsernameAndIsActiveAndIsDeleted(userName, Constant.IsActive.TRUE, Constant.IsDeleted.FALSE);

        Assert.notNull(account, MessageUtils.getMessage(Constant.MessageCode.ERR_ACCOUNT_NOT_FOUND));

        return Transformer.transformToAccountDto(account, true);
    }

    @Override
    public Account saveAndFlush(Account account) {
        return repository.saveAndFlush(account);
    }

    @Override
    public Optional<Account> findById(Long accountId) {
        return repository.findById(accountId);
    }

    @Override
    public Long countByRoleAndIsDeletedAndGender(String role, Boolean isDeleted, String gender) {
        return repository.countByRoleAndIsDeletedAndGender(role, isDeleted, gender);
    }
}
