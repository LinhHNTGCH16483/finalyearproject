package com.example.eschool.service.impl;

import com.example.eschool.model.entity.Submission;
import com.example.eschool.repository.database.SubmissionRepository;
import com.example.eschool.service.SubmissionService;
import org.springframework.stereotype.Service;

@Service
public class SubmissionServiceImpl extends BaseServiceImpl<Submission, Long, SubmissionRepository> implements SubmissionService {

    public SubmissionServiceImpl(SubmissionRepository repository) {
        super(repository);
    }

}
