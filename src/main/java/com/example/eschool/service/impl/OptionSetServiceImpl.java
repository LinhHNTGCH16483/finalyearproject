package com.example.eschool.service.impl;

import com.example.eschool.model.entity.OptionSet;
import com.example.eschool.repository.database.OptionSetRepository;
import com.example.eschool.service.OptionSetService;
import org.springframework.stereotype.Service;

@Service
public class OptionSetServiceImpl extends BaseServiceImpl<OptionSet, Long, OptionSetRepository> implements OptionSetService {

    public OptionSetServiceImpl(OptionSetRepository repository) {
        super(repository);
    }

}
