package com.example.eschool.service.impl;

import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Major;
import com.example.eschool.repository.database.MajorRepository;
import com.example.eschool.service.MajorService;
import org.springframework.stereotype.Service;

@Service
public class MajorServiceImpl extends BaseServiceImpl<Major, Long, MajorRepository> implements MajorService {

    public MajorServiceImpl(MajorRepository repository) {
        super(repository);
    }

    @Override
    public CountObjectResponseDTO countMajor() {
        return CountObjectResponseDTO.builder()
                .total(repository.countByIsDeleted(false))
                .build();
    }
}
