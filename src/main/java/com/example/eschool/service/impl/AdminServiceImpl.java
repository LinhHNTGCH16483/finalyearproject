package com.example.eschool.service.impl;

import com.example.eschool.model.entity.Admin;
import com.example.eschool.repository.database.AdminRepository;
import com.example.eschool.service.AdminService;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl extends BaseServiceImpl<Admin, Long, AdminRepository> implements AdminService {

    public AdminServiceImpl(AdminRepository repository) {
        super(repository);
    }

}
