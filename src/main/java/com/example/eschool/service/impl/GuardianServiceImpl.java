package com.example.eschool.service.impl;

import com.example.eschool.common.Constant;
import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Guardian;
import com.example.eschool.repository.database.GuardianRepository;
import com.example.eschool.service.AccountService;
import com.example.eschool.service.GuardianService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GuardianServiceImpl extends BaseServiceImpl<Guardian, Long, GuardianRepository> implements GuardianService {

    @Autowired
    AccountService accountService;

    public GuardianServiceImpl(GuardianRepository repository) {
        super(repository);
    }

    @Override
    public CountObjectResponseDTO countGuardian() {
        return CountObjectResponseDTO.builder()
                .total(repository.countByIsDeleted(false))
                .male(accountService.countByRoleAndIsDeletedAndGender(Constant.Role.GUARDIAN, false, Constant.Gender.MALE))
                .female(accountService.countByRoleAndIsDeletedAndGender(Constant.Role.GUARDIAN, false, Constant.Gender.FEMALE))
                .otherGender(accountService.countByRoleAndIsDeletedAndGender(Constant.Role.GUARDIAN, false, Constant.Gender.OTHER))
                .build();
    }
}
