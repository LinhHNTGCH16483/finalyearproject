package com.example.eschool.service.impl;

import com.example.eschool.common.Constant;
import com.example.eschool.common.MessageUtils;
import com.example.eschool.model.dto.FileDTO;
import com.example.eschool.model.entity.File;
import com.example.eschool.repository.database.FileRepository;
import com.example.eschool.service.FileService;
import com.example.eschool.service.MinioService;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class FileServiceImpl extends BaseServiceImpl<File, Long, FileRepository> implements FileService {

    @Autowired
    private MinioService minioService;

    public FileServiceImpl(FileRepository repository) {
        super(repository);
    }

    @Override
    public FileDTO uploadFile(MultipartFile file) throws IOException {

        String bucketName = ThreadContext.get(Constant.ThreadContextKey.USERNAME);

        Assert.notNull(bucketName, MessageUtils.getMessage(Constant.MessageCode.ERR_BUCKET_NAME_NOT_FOUND));

        FileDTO fileDTO = minioService.uploadFile(file, bucketName);

        File fileEntity = File.builder()
                .bucketName(fileDTO.getBucketName())
                .name(fileDTO.getFilename())
                .type(fileDTO.getType())
                .size(fileDTO.getSize())
                .extension(fileDTO.getExtension())
                .isDelete(false)
                .build();

        save(fileEntity);

        fileEntity = repository.findByBucketNameAndNameAndIsDelete(bucketName, fileDTO.getFilename(), false);

        fileDTO.setId(fileEntity.getId());
        fileDTO.setIsDelete(fileEntity.getIsDelete());
        fileDTO.setCreatedBy(fileEntity.getCreatedBy());
        fileDTO.setCreatedDate(fileEntity.getCreatedDate());
        fileDTO.setLastModifiedBy(fileEntity.getLastModifiedBy());
        fileDTO.setLastModifiedDate(fileEntity.getLastModifiedDate());

        return fileDTO;
    }

    @Override
    public byte[] downloadFile(String bucketName, String filename) throws IOException {
        return minioService.downloadFile(filename, bucketName);
    }
}
