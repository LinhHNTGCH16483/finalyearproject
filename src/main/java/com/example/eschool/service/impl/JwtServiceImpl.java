package com.example.eschool.service.impl;

import com.example.eschool.common.Constant.ClaimsKey;
import com.example.eschool.common.Constant.JwtInfo;
import com.example.eschool.service.JwtService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
public class JwtServiceImpl implements JwtService {

    @Override
    public Claims getClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(JwtInfo.SECRET_KEY).parseClaimsJws(token).getBody();
    }

    @Override
    public String getUsernameFromToken(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    @Override
    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(ClaimsKey.USERNAME, userDetails.getUsername());
        claims.put(ClaimsKey.ROLE, userDetails.getAuthorities().toArray()[0].toString());
        claims.put(ClaimsKey.IS_ACTIVE, userDetails.isEnabled());
        return createToken(claims, userDetails.getUsername());
    }

    @Override
    public Boolean validateToken(String token, UserDetails userDetails) {
        String username = getUsernameFromToken(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    private <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        Claims claims = getClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    private String createToken(Map<String, Object> claims, String subject) {

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JwtInfo.EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, JwtInfo.SECRET_KEY)
                .compact();
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }
}
