package com.example.eschool.service.impl;

import com.example.eschool.model.entity.Blog;
import com.example.eschool.repository.database.BlogRepository;
import com.example.eschool.service.BlogService;
import org.springframework.stereotype.Service;

@Service
public class BlogServiceImpl extends BaseServiceImpl<Blog, Long, BlogRepository> implements BlogService {

    public BlogServiceImpl(BlogRepository repository) {
        super(repository);
    }

}
