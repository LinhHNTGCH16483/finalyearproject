package com.example.eschool.service.impl;

import com.example.eschool.common.Constant;
import com.example.eschool.common.Constant.EmailInfo;
import com.example.eschool.common.MessageUtils;
import com.example.eschool.model.dto.StudentDTO;
import com.example.eschool.model.entity.Account;
import com.example.eschool.model.entity.Class;
import com.example.eschool.model.entity.Lecturer;
import com.example.eschool.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    JavaMailSender javaMailSender;

    @Autowired
    ClassService classService;

    @Autowired
    LecturerService lecturerService;

    @Autowired
    AccountService accountService;

    @Autowired
    ClassStudentService classStudentService;

    @Override
    public void sendMailToClassMember(Long classId, Constant.ScheduleStatus status) {
        Class clazz = classService.findById(classId).orElse(null);
        Assert.notNull(clazz, MessageUtils.getMessage(Constant.MessageCode.ERR_CLASS_NOT_FOUND));

        Lecturer lecturer = lecturerService.findById(clazz.getLecturerId()).orElse(null);
        Assert.notNull(lecturer, MessageUtils.getMessage(Constant.MessageCode.ERR_LECTURER_NOT_FOUND));

        Account account = accountService.findById(lecturer.getAccountId()).orElse(null);
        Assert.notNull(account, MessageUtils.getMessage(Constant.MessageCode.ERR_ACCOUNT_NOT_FOUND));

        String content = createContent(account.getFullName(), clazz.getCode(), status);
        this.sendEmail(account.getEmail(), content);

        List<StudentDTO> studentDTOList = classStudentService.getAllStudentByClassId(classId);
        for (StudentDTO dto : studentDTOList) {
            content = createContent(dto.getFullName(), clazz.getCode(), status);
            this.sendEmail(dto.getEmail(), content);
        }
    }

    private String createContent(String fullName, String classCode, Constant.ScheduleStatus status) {
        if (Constant.ScheduleStatus.CREATE.equals(status)) {
            return "Dear " + fullName + ",\n" + "You have a new slot in the class " + classCode + ".";
        } else if (Constant.ScheduleStatus.UPDATE.equals(status)) {
            return "Dear " + fullName + ",\n" + "Your slot in the class " + classCode + " has been updated.";
        } else {
            return "Dear " + fullName + ",\n" + "Your slot in the class " + classCode + " has been deleted.";
        }
    }

    @Override
    public void sendEmail(String receiver, String content) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(receiver);
        message.setSubject(EmailInfo.SUBJECT);
        message.setText(content);
        javaMailSender.send(message);
    }
}
