package com.example.eschool.service.impl;

import com.example.eschool.common.Constant;
import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Lecturer;
import com.example.eschool.repository.database.LecturerRepository;
import com.example.eschool.service.AccountService;
import com.example.eschool.service.LecturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LecturerServiceImpl extends BaseServiceImpl<Lecturer, Long, LecturerRepository> implements LecturerService {

    @Autowired
    AccountService accountService;

    public LecturerServiceImpl(LecturerRepository repository) {
        super(repository);
    }

    @Override
    public Optional<Lecturer> findById(Long lecturerId) {
        return repository.findById(lecturerId);
    }

    @Override
    public CountObjectResponseDTO countLecturer() {
        return CountObjectResponseDTO.builder()
                .total(repository.countByIsDeleted(false))
                .male(accountService.countByRoleAndIsDeletedAndGender(Constant.Role.LECTURER, false, Constant.Gender.MALE))
                .female(accountService.countByRoleAndIsDeletedAndGender(Constant.Role.LECTURER, false, Constant.Gender.FEMALE))
                .otherGender(accountService.countByRoleAndIsDeletedAndGender(Constant.Role.LECTURER, false, Constant.Gender.OTHER))
                .build();
    }
}
