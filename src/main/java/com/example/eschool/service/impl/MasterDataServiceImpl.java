package com.example.eschool.service.impl;

import com.example.eschool.common.Transformer;
import com.example.eschool.model.dto.MasterDataDTO;
import com.example.eschool.model.entity.MasterData;
import com.example.eschool.repository.database.MasterDataRepository;
import com.example.eschool.service.MasterDataService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MasterDataServiceImpl extends BaseServiceImpl<MasterData, Long, MasterDataRepository> implements MasterDataService {

    public MasterDataServiceImpl(MasterDataRepository repository) {
        super(repository);
    }

    @Override
    public List<MasterDataDTO> getMasterData(String lang) {
        List<MasterData> masterDataList = repository.findAllByLangAndIsDeleted(lang, Boolean.FALSE);
        return masterDataList.stream()
                .map(masterData -> Transformer.transformToMasterDataDTO(masterData, false))
                .collect(Collectors.toList());
    }
}
