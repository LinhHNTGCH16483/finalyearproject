package com.example.eschool.service.impl;

import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Class;
import com.example.eschool.repository.database.ClassRepository;
import com.example.eschool.service.ClassService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClassServiceImpl extends BaseServiceImpl<Class, Long, ClassRepository> implements ClassService {

    public ClassServiceImpl(ClassRepository repository) {
        super(repository);
    }

    @Override
    public Optional<Class> findById(Long classId) {
        return repository.findById(classId);
    }

    @Override
    public CountObjectResponseDTO countClass() {
        return CountObjectResponseDTO.builder()
                .total(repository.countByIsDeleted(false))
                .build();
    }

    @Override
    public Boolean existsById(Long classId) {
        return repository.existsById(classId);
    }

    @Override
    public List<Class> findAllByLecturerId(Long lecturerId) {
        return repository.findAllByLecturerIdAndIsDeleted(lecturerId, false);
    }
}
