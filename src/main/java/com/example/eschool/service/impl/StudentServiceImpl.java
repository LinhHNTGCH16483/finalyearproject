package com.example.eschool.service.impl;

import com.example.eschool.common.Constant;
import com.example.eschool.common.DataUtils;
import com.example.eschool.common.MessageUtils;
import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.dto.SearchStudentResponseDTO;
import com.example.eschool.model.dto.StudentDTO;
import com.example.eschool.model.entity.Account;
import com.example.eschool.model.entity.Student;
import com.example.eschool.repository.database.AccountRepository;
import com.example.eschool.repository.database.StudentRepository;
import com.example.eschool.service.AccountService;
import com.example.eschool.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class StudentServiceImpl extends BaseServiceImpl<Student, Long, StudentRepository> implements StudentService {

    @Autowired
    AccountService accountService;

    @Autowired
    PasswordEncoder passwordEncoder;

    public StudentServiceImpl(StudentRepository repository) {
        super(repository);
    }

    @Override
    public SearchStudentResponseDTO searchStudent(String studentCode, String studentName, Long pageSize, Long pageNumber) {
        List<StudentDTO> studentDTOList = repository.searchStudent(studentCode, studentName, pageSize, pageNumber);
        Long countStudentDTO = repository.countStudent(studentCode, studentName, pageSize, pageNumber);
        return SearchStudentResponseDTO.builder()
                .totalRecords(countStudentDTO)
                .studentDTOList(studentDTOList)
                .build();
    }

    @Override
    public Boolean addStudent(StudentDTO studentDTO) {
        Account account = Account.builder()
                .username(studentDTO.getUsername())
                .password(passwordEncoder.encode(studentDTO.getPassword()))
                .role(studentDTO.getRole())
                .isActive(studentDTO.getIsAccountActive())
                .firstName(studentDTO.getFirstName())
                .lastName(studentDTO.getLastName())
                .fullName(studentDTO.getFirstName() + " " + studentDTO.getLastName())
                .dateOfBirth(studentDTO.getDateOfBirth())
                .placeOfBirth(studentDTO.getPlaceOfBirth())
                .gender(studentDTO.getGender())
                .nationality(studentDTO.getNationality())
                .religion(studentDTO.getReligion())
                .maritalStatus(studentDTO.getMaritalStatus())
                .permanentAddress(studentDTO.getPermanentAddress())
                .phoneNumber(studentDTO.getPhoneNumber())
                .email(studentDTO.getEmail())
                .avatarImage(studentDTO.getAvatarImage())
                .isDeleted(false)
                .build();
        account = accountService.saveAndFlush(account);

        Student student = Student.builder()
                .accountId(account.getId())
                .code(DataUtils.generateStudentCode())
                .admissionDate(studentDTO.getAdmissionDate())
                .isDeleted(false)
                .build();
        save(student);

        return true;
    }

    @Override
    public Boolean updateStudent(Long studentId, StudentDTO studentDTO) {
        Account account = accountService.findById(studentDTO.getAccountId()).orElse(null);
        Assert.notNull(account, MessageUtils.getMessage(Constant.MessageCode.ERR_ACCOUNT_NOT_FOUND));

        account.setPassword(DataUtils.isNullOrEmpty(studentDTO.getPassword()) ? account.getPassword() : passwordEncoder.encode(studentDTO.getPassword()));
        account.setIsActive(DataUtils.isNull(studentDTO.getIsAccountActive()) ? account.getIsActive() : studentDTO.getIsAccountActive());
        account.setFirstName(DataUtils.isNullOrEmpty(studentDTO.getFirstName()) ? account.getFirstName() : studentDTO.getFirstName());
        account.setLastName(DataUtils.isNullOrEmpty(studentDTO.getLastName()) ? account.getLastName() : studentDTO.getLastName());
        account.setFullName(DataUtils.isNullOrEmpty(studentDTO.getFullName()) ? account.getFullName() : studentDTO.getFirstName() + " " + studentDTO.getLastName());
        account.setDateOfBirth(DataUtils.isNull(studentDTO.getDateOfBirth()) ? account.getDateOfBirth() : studentDTO.getDateOfBirth());
        account.setPlaceOfBirth(DataUtils.isNull(studentDTO.getPlaceOfBirth()) ? account.getPlaceOfBirth() : studentDTO.getPlaceOfBirth());
        account.setGender(DataUtils.isNullOrEmpty(studentDTO.getGender()) ? account.getGender() : studentDTO.getGender());
        account.setNationality(DataUtils.isNullOrEmpty(studentDTO.getNationality()) ? account.getNationality() : studentDTO.getNationality());
        account.setReligion(DataUtils.isNullOrEmpty(studentDTO.getReligion()) ? account.getReligion() : studentDTO.getReligion());
        account.setMaritalStatus(DataUtils.isNullOrEmpty(studentDTO.getMaritalStatus()) ? account.getMaritalStatus() : studentDTO.getMaritalStatus());
        account.setPermanentAddress(DataUtils.isNullOrEmpty(studentDTO.getPermanentAddress()) ? account.getPermanentAddress() : studentDTO.getPermanentAddress());
        account.setPhoneNumber(DataUtils.isNullOrEmpty(studentDTO.getPhoneNumber()) ? account.getPhoneNumber() : studentDTO.getPhoneNumber());
        account.setEmail(DataUtils.isNullOrEmpty(studentDTO.getEmail()) ? account.getEmail() : studentDTO.getEmail());
        account.setAvatarImage(DataUtils.isNullOrEmpty(studentDTO.getAvatarImage()) ? account.getAvatarImage() : studentDTO.getAvatarImage());
        account.setIsDeleted(DataUtils.isNull(studentDTO.getIsAccountDeleted()) ? account.getIsDeleted() : studentDTO.getIsAccountDeleted());

        Student student = repository.findById(studentId).orElse(null);
        Assert.notNull(student, MessageUtils.getMessage(Constant.MessageCode.ERR_STUDENT_NOT_FOUND));

        student.setAdmissionDate(DataUtils.isNull(studentDTO.getAdmissionDate()) ? student.getAdmissionDate() : studentDTO.getAdmissionDate());
        student.setIsDeleted(DataUtils.isNull(studentDTO.getIsDeleted()) ? student.getIsDeleted() : studentDTO.getIsDeleted());

        accountService.save(account);
        update(student);
        return true;
    }

    @Override
    public Boolean deleteStudent(Long studentId) {
        Student student = repository.findById(studentId).orElse(null);
        Assert.notNull(student, MessageUtils.getMessage(Constant.MessageCode.ERR_STUDENT_NOT_FOUND));

        student.setIsDeleted(true);

        Account account = accountService.findById(student.getAccountId()).orElse(null);
        Assert.notNull(account, MessageUtils.getMessage(Constant.MessageCode.ERR_ACCOUNT_NOT_FOUND));

        account.setIsDeleted(true);

        update(student);
        accountService.save(account);
        return true;
    }

    @Override
    public CountObjectResponseDTO countStudent() {
        return CountObjectResponseDTO.builder()
                .total(repository.countByIsDeleted(false))
                .male(accountService.countByRoleAndIsDeletedAndGender(Constant.Role.STUDENT, false, Constant.Gender.MALE))
                .female(accountService.countByRoleAndIsDeletedAndGender(Constant.Role.STUDENT, false, Constant.Gender.FEMALE))
                .otherGender(accountService.countByRoleAndIsDeletedAndGender(Constant.Role.STUDENT, false, Constant.Gender.OTHER))
                .build();
    }
}
