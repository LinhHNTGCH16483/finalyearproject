package com.example.eschool.service.impl;

import com.example.eschool.common.Constant.MessageCode;
import com.example.eschool.service.TestService;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import static com.example.eschool.common.MessageUtils.getMessage;

@Service
public class TestServiceImpl implements TestService {

    @Override
    public String test(String a) {
        return a;
    }
}
