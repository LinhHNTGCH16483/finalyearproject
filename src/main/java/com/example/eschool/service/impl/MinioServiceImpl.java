package com.example.eschool.service.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.example.eschool.model.dto.FileDTO;
import com.example.eschool.service.MinioService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static com.example.eschool.common.Constant.FILE_EXTENSION_LIST;
import static com.example.eschool.common.Constant.MessageCode;
import static com.example.eschool.common.DataUtils.formatFilename;
import static com.example.eschool.common.DataUtils.notNullOrEmpty;
import static com.example.eschool.common.MessageUtils.getMessage;

@Service
public class MinioServiceImpl implements MinioService {

    @Autowired
    private AmazonS3 minioClient;

    @Override
    public FileDTO uploadFile(MultipartFile file, String bucketName) throws IOException {

        String fileExtension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.') + 1);

        Assert.isTrue(FILE_EXTENSION_LIST.contains(fileExtension), getMessage(MessageCode.ERR_INVALID_FILE_NAME));

        if (!minioClient.doesBucketExist(bucketName)) {
            minioClient.createBucket(bucketName);
        }

        String filename = String.format("%s_%s_%s",
                LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE),
                System.currentTimeMillis(),
                formatFilename(file.getOriginalFilename()));
        long size = file.getSize();
        String type = file.getContentType();
        InputStream inputStream = new ByteArrayInputStream(file.getBytes());
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType(type);

        minioClient.putObject(bucketName, filename, inputStream, objectMetadata);

        return FileDTO.builder()
                .bucketName(bucketName)
                .filename(filename)
                .type(type)
                .size(size)
                .extension(fileExtension)
                .build();
    }

    @Override
    public byte[] downloadFile(String filename, String bucketName) throws IOException {

        filename = formatFilename(filename);

        Assert.isTrue(minioClient.doesObjectExist(bucketName, filename), getMessage(MessageCode.ERR_FILE_NOT_FOUND, filename));

        S3Object s3Object = minioClient.getObject(bucketName, filename);
        S3ObjectInputStream inputStream = s3Object.getObjectContent();
        return IOUtils.toByteArray(inputStream);
    }

    @Override
    public Boolean deleteFile(String filename, String bucketName) {

        filename = formatFilename(filename);

        Assert.isTrue(minioClient.doesObjectExist(bucketName, filename), getMessage(MessageCode.ERR_FILE_NOT_FOUND, filename));

        try {
            minioClient.deleteObject(bucketName, filename);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
}
