package com.example.eschool.service.impl;

import com.example.eschool.model.entity.Menu;
import com.example.eschool.repository.database.MenuRepository;
import com.example.eschool.service.MenuService;
import org.springframework.stereotype.Service;

@Service
public class MenuServiceImpl extends BaseServiceImpl<Menu, Long, MenuRepository> implements MenuService {

    public MenuServiceImpl(MenuRepository repository) {
        super(repository);
    }

}
