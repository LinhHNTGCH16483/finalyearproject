package com.example.eschool.service.impl;

import com.example.eschool.common.Constant;
import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Manager;
import com.example.eschool.repository.database.ManagerRepository;
import com.example.eschool.service.AccountService;
import com.example.eschool.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ManagerServiceImpl extends BaseServiceImpl<Manager, Long, ManagerRepository> implements ManagerService {

    @Autowired
    AccountService accountService;

    public ManagerServiceImpl(ManagerRepository repository) {
        super(repository);
    }

    @Override
    public CountObjectResponseDTO countManager() {
        return CountObjectResponseDTO.builder()
                .total(repository.countByIsDeleted(false))
                .male(accountService.countByRoleAndIsDeletedAndGender(Constant.Role.MANAGER, false, Constant.Gender.MALE))
                .female(accountService.countByRoleAndIsDeletedAndGender(Constant.Role.MANAGER, false, Constant.Gender.FEMALE))
                .otherGender(accountService.countByRoleAndIsDeletedAndGender(Constant.Role.MANAGER, false, Constant.Gender.OTHER))
                .build();
    }
}
