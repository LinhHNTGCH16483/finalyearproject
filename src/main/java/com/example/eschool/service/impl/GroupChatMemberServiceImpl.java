package com.example.eschool.service.impl;

import com.example.eschool.common.Constant;
import com.example.eschool.model.entity.GroupChatMember;
import com.example.eschool.repository.database.GroupChatMemberRepository;
import com.example.eschool.service.GroupChatMemberService;
import org.springframework.stereotype.Service;

@Service
public class GroupChatMemberServiceImpl extends BaseServiceImpl<GroupChatMember, Long, GroupChatMemberRepository> implements GroupChatMemberService {

    public GroupChatMemberServiceImpl(GroupChatMemberRepository repository) {
        super(repository);
    }

    @Override
    public boolean isMember(Long groupId, Long memberId) {
        return repository.existsByGroupChatIdAndAccountIdAndIsDeleted(groupId, memberId, Constant.IsDeleted.FALSE);
    }
}
