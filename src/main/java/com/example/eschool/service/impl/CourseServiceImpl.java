package com.example.eschool.service.impl;

import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Course;
import com.example.eschool.repository.database.CourseRepository;
import com.example.eschool.service.CourseService;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl extends BaseServiceImpl<Course, Long, CourseRepository> implements CourseService {

    public CourseServiceImpl(CourseRepository repository) {
        super(repository);
    }

    @Override
    public CountObjectResponseDTO countCourse() {
        return CountObjectResponseDTO.builder()
                .total(repository.countByIsDeleted(false))
                .build();
    }
}
