package com.example.eschool.service.impl;

import com.example.eschool.model.entity.Assignment;
import com.example.eschool.repository.database.AssignmentRepository;
import com.example.eschool.service.AssignmentService;
import org.springframework.stereotype.Service;

@Service
public class AssignmentServiceImpl extends BaseServiceImpl<Assignment, Long, AssignmentRepository> implements AssignmentService {

    public AssignmentServiceImpl(AssignmentRepository repository) {
        super(repository);
    }

}
