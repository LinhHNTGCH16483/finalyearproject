package com.example.eschool.service.impl;

import com.example.eschool.common.Transformer;
import com.example.eschool.model.dto.ClassStudentDTO;
import com.example.eschool.model.dto.StudentDTO;
import com.example.eschool.model.entity.Account;
import com.example.eschool.model.entity.ClassStudent;
import com.example.eschool.model.entity.Student;
import com.example.eschool.repository.database.AccountRepository;
import com.example.eschool.repository.database.ClassStudentRepository;
import com.example.eschool.repository.database.StudentRepository;
import com.example.eschool.service.ClassStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClassStudentServiceImpl extends BaseServiceImpl<ClassStudent, Long, ClassStudentRepository> implements ClassStudentService {

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    AccountRepository accountRepository;

    public ClassStudentServiceImpl(ClassStudentRepository repository) {
        super(repository);
    }

    @Override
    public List<StudentDTO> getAllStudentByClassId(Long classId) {

        List<ClassStudent> classStudentList = repository.findAllByClassIdAndIsDeleted(classId, false);

        List<Long> idList = classStudentList.stream().map(ClassStudent::getStudentId).collect(Collectors.toList());

        List<Student> studentList = studentRepository.findAllByIdIsInAndIsDeleted(idList, false);

        idList = studentList.stream().map(Student::getAccountId).collect(Collectors.toList());

        List<Account> accountList = accountRepository.findAllByIdIsInAndIsDeleted(idList, false);

        return Transformer.transformToListStudentDTO(studentList, accountList);
    }

    @Override
    public List<ClassStudentDTO> findAllByStudentId(Long studentId) {
        List<ClassStudent> classStudentList = repository.findAllByStudentIdAndIsDeleted(studentId, false);
        return Transformer.transformToListClassStudentDTO(classStudentList);
    }
}
