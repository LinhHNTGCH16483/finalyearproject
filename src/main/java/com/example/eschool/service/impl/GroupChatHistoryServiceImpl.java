package com.example.eschool.service.impl;

import com.example.eschool.common.Constant;
import com.example.eschool.model.dto.AccountDTO;
import com.example.eschool.model.dto.MessageDTO;
import com.example.eschool.model.entity.GroupChatHistory;
import com.example.eschool.repository.database.GroupChatHistoryRepository;
import com.example.eschool.service.AccountService;
import com.example.eschool.service.GroupChatHistoryService;
import com.example.eschool.service.GroupChatMemberService;
import com.example.eschool.service.GroupChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class GroupChatHistoryServiceImpl extends BaseServiceImpl<GroupChatHistory, Long, GroupChatHistoryRepository> implements GroupChatHistoryService {

    @Autowired
    private AccountService accountService;

    @Autowired
    private GroupChatService groupChatService;

    @Autowired
    private GroupChatMemberService groupChatMemberService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    public GroupChatHistoryServiceImpl(GroupChatHistoryRepository repository) {
        super(repository);
    }

    @Override
    public void sendMessage(Long groupChatId, MessageDTO messageDTO) {
        Assert.isTrue(groupChatService.existById(groupChatId), Constant.MessageCode.GROUP_CHAT_NOT_EXIST);

        AccountDTO accountDTO = accountService.findByUserName(messageDTO.getUsername());
        Assert.notNull(accountDTO, Constant.MessageCode.ERR_WRONG_USERNAME_PASSWORD);

        Assert.isTrue(groupChatMemberService.isMember(groupChatId, accountDTO.getId()), Constant.MessageCode.GROUP_CHAT_IS_NOT_MEMBER);

        GroupChatHistory groupChatHistory = GroupChatHistory.builder()
                .groupChatId(groupChatId)
                .groupChatMemberId(accountDTO.getId())
                .content(messageDTO.getContent())
                .build();
        save(groupChatHistory);

        simpMessagingTemplate.convertAndSend("/topic/" + groupChatId, messageDTO);
    }
}
