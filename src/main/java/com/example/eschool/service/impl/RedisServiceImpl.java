package com.example.eschool.service.impl;

import com.example.eschool.common.Constant;
import com.example.eschool.common.DataUtils;
import com.example.eschool.model.redis.TokenRedis;
import com.example.eschool.repository.redis.TokenRepository;
import com.example.eschool.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import static com.example.eschool.common.MessageUtils.getMessage;

@Service
public class RedisServiceImpl implements RedisService {

    @Autowired
    TokenRepository tokenRepository;

    @Override
    public Boolean addToken(TokenRedis tokenRedis) {
        tokenRepository.save(tokenRedis);
        return true;
    }

    @Override
    public Boolean updateToken(String username, String newToken) {
        TokenRedis tokenRedis = tokenRepository.findByUsername(username);
        Assert.notNull(tokenRedis, getMessage(Constant.MessageCode.ERR_TOKEN_NOT_FOUND));
        tokenRedis.setToken(newToken);
        tokenRepository.save(tokenRedis);
        return true;
    }

    @Override
    public Boolean deleteToken(String username) {
        TokenRedis tokenRedis = tokenRepository.findByUsername(username);
        if (DataUtils.notNull(tokenRedis)) {
            tokenRepository.deleteById(tokenRedis.getId());
        }
        return true;
    }

    @Override
    public TokenRedis findTokenByUsername(String username) {
        TokenRedis tokenRedis = tokenRepository.findByUsername(username);
        if (DataUtils.notNull(tokenRedis)) {
            return tokenRedis;
        }
        return null;
    }
}
