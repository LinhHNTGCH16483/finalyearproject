package com.example.eschool.service.impl;

import com.example.eschool.model.entity.Feedback;
import com.example.eschool.repository.database.FeedbackRepository;
import com.example.eschool.service.FeedbackService;
import org.springframework.stereotype.Service;

@Service
public class FeedbackServiceImpl extends BaseServiceImpl<Feedback, Long, FeedbackRepository> implements FeedbackService {

    public FeedbackServiceImpl(FeedbackRepository repository) {
        super(repository);
    }

}
