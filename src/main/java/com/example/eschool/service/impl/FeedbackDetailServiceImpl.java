package com.example.eschool.service.impl;

import com.example.eschool.model.entity.FeedbackDetail;
import com.example.eschool.repository.database.FeedbackDetailRepository;
import com.example.eschool.service.FeedbackDetailService;
import org.springframework.stereotype.Service;

@Service
public class FeedbackDetailServiceImpl extends BaseServiceImpl<FeedbackDetail, Long, FeedbackDetailRepository> implements FeedbackDetailService {

    public FeedbackDetailServiceImpl(FeedbackDetailRepository repository) {
        super(repository);
    }

}
