package com.example.eschool.service.impl;

import com.example.eschool.service.BaseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public class BaseServiceImpl<Entity, ID, Repository extends JpaRepository<Entity, ID>> implements BaseService<Entity, ID> {

    protected final Repository repository;

    public BaseServiceImpl(Repository repository) {
        this.repository = repository;
    }

    @Override
    public boolean existById(ID id) {
        return repository.existsById(id);
    }

    @Override
    public Entity save(Entity entity) {
        return repository.saveAndFlush(entity);
    }

    @Override
    public Entity getById(ID id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Entity update(Entity entity) {
        return repository.save(entity);
    }

    @Override
    public void delete(ID id) {
        repository.deleteById(id);
    }

    @Override
    public List<Entity> getAll() {
        return repository.findAll();
    }

    @Override
    public Page<Entity> getPageWithOrders(Integer pageNumber, Integer pageSize, List<Order> orders) {
        return repository.findAll(PageRequest.of(pageNumber, pageSize, Sort.by(orders)));
    }

    @Override
    public Page<Entity> getPageWithoutOrders(Integer pageNumber, Integer pageSize) {
        return repository.findAll(PageRequest.of(pageNumber, pageSize));
    }
}
