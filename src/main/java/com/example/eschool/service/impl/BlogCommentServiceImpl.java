package com.example.eschool.service.impl;

import com.example.eschool.model.entity.BlogComment;
import com.example.eschool.repository.database.BlogCommentRepository;
import com.example.eschool.service.BlogCommentService;
import org.springframework.stereotype.Service;

@Service
public class BlogCommentServiceImpl extends BaseServiceImpl<BlogComment, Long, BlogCommentRepository> implements BlogCommentService {

    public BlogCommentServiceImpl(BlogCommentRepository repository) {
        super(repository);
    }

}
