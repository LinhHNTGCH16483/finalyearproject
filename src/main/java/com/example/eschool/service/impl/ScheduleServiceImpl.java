package com.example.eschool.service.impl;

import com.example.eschool.common.Constant;
import com.example.eschool.common.DataUtils;
import com.example.eschool.common.MessageUtils;
import com.example.eschool.common.Transformer;
import com.example.eschool.model.dto.CalendarDTO;
import com.example.eschool.model.dto.ClassStudentDTO;
import com.example.eschool.model.dto.ScheduleDTO;
import com.example.eschool.model.dto.SlotDTO;
import com.example.eschool.model.entity.Class;
import com.example.eschool.model.entity.Schedule;
import com.example.eschool.repository.database.ScheduleRepository;
import com.example.eschool.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Service
public class ScheduleServiceImpl extends BaseServiceImpl<Schedule, Long, ScheduleRepository> implements ScheduleService {

    @Autowired
    ClassService classService;

    @Autowired
    LecturerService lecturerService;

    @Autowired
    EmailService emailService;

    @Autowired
    ClassStudentService classStudentService;

    @Autowired
    StudentService studentService;

    public ScheduleServiceImpl(ScheduleRepository repository) {
        super(repository);
    }

    @Override
    public Boolean addSchedule(ScheduleDTO scheduleDTO) {

        Assert.isTrue(classService.existsById(scheduleDTO.getClassId()), MessageUtils.getMessage(Constant.MessageCode.ERR_CLASS_NOT_FOUND));

        List<Boolean> slotList = new ArrayList<>(8);
        slotList.add(scheduleDTO.getSlot1());
        slotList.add(scheduleDTO.getSlot2());
        slotList.add(scheduleDTO.getSlot3());
        slotList.add(scheduleDTO.getSlot4());
        slotList.add(scheduleDTO.getSlot5());
        slotList.add(scheduleDTO.getSlot6());
        slotList.add(scheduleDTO.getSlot7());
        slotList.add(scheduleDTO.getSlot8());
        long tmp = slotList.stream().filter(item -> item).count();
        Assert.isTrue(1 == tmp, MessageUtils.getMessage(Constant.MessageCode.ERR_BOOK_MORE_THAN_ONE_SLOT));

        Schedule schedule = Schedule.builder()
                .classId(scheduleDTO.getClassId())
                .date(scheduleDTO.getDate())
                .slot1(scheduleDTO.getSlot1())
                .slot2(scheduleDTO.getSlot2())
                .slot3(scheduleDTO.getSlot3())
                .slot4(scheduleDTO.getSlot4())
                .slot5(scheduleDTO.getSlot5())
                .slot6(scheduleDTO.getSlot6())
                .slot7(scheduleDTO.getSlot7())
                .slot8(scheduleDTO.getSlot8())
                .isDeleted(false)
                .build();

        Assert.isTrue(
                !repository.existsByClassIdAndDateAndSlot1AndSlot2AndSlot3AndSlot4AndSlot5AndSlot6AndSlot7AndSlot8AndIsDeleted(
                        schedule.getClassId(),
                        schedule.getDate(),
                        schedule.getSlot1(),
                        schedule.getSlot2(),
                        schedule.getSlot3(),
                        schedule.getSlot4(),
                        schedule.getSlot5(),
                        schedule.getSlot6(),
                        schedule.getSlot7(),
                        schedule.getSlot8(),
                        false),
                MessageUtils.getMessage(Constant.MessageCode.ERR_SLOT_IS_BOOKED));

        save(schedule);

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(() -> emailService.sendMailToClassMember(schedule.getClassId(), Constant.ScheduleStatus.CREATE));

        return true;
    }

    @Override
    public Boolean updateSchedule(Long scheduleId, ScheduleDTO scheduleDTO) {
        Schedule schedule = repository.findById(scheduleId).orElse(null);
        Assert.notNull(schedule, MessageUtils.getMessage(Constant.MessageCode.ERR_SCHEDULE_NOT_FOUND));

        List<Boolean> slotList = new ArrayList<>(8);
        slotList.add(schedule.getSlot1());
        slotList.add(schedule.getSlot2());
        slotList.add(schedule.getSlot3());
        slotList.add(schedule.getSlot4());
        slotList.add(schedule.getSlot5());
        slotList.add(schedule.getSlot6());
        slotList.add(schedule.getSlot7());
        slotList.add(schedule.getSlot8());
        long tmp = slotList.stream().filter(item -> item).count();
        Assert.isTrue(1 == tmp, MessageUtils.getMessage(Constant.MessageCode.ERR_BOOK_MORE_THAN_ONE_SLOT));

        schedule.setDate(DataUtils.isNull(scheduleDTO.getDate()) ? schedule.getDate() : scheduleDTO.getDate());
        schedule.setSlot1(DataUtils.isNull(scheduleDTO.getSlot1()) ? schedule.getSlot1() : scheduleDTO.getSlot1());
        schedule.setSlot2(DataUtils.isNull(scheduleDTO.getSlot2()) ? schedule.getSlot2() : scheduleDTO.getSlot2());
        schedule.setSlot3(DataUtils.isNull(scheduleDTO.getSlot3()) ? schedule.getSlot3() : scheduleDTO.getSlot3());
        schedule.setSlot4(DataUtils.isNull(scheduleDTO.getSlot4()) ? schedule.getSlot4() : scheduleDTO.getSlot4());
        schedule.setSlot5(DataUtils.isNull(scheduleDTO.getSlot5()) ? schedule.getSlot5() : scheduleDTO.getSlot5());
        schedule.setSlot6(DataUtils.isNull(scheduleDTO.getSlot6()) ? schedule.getSlot6() : scheduleDTO.getSlot6());
        schedule.setSlot7(DataUtils.isNull(scheduleDTO.getSlot7()) ? schedule.getSlot7() : scheduleDTO.getSlot7());
        schedule.setSlot8(DataUtils.isNull(scheduleDTO.getSlot8()) ? schedule.getSlot8() : scheduleDTO.getSlot8());
        schedule.setIsDeleted(DataUtils.isNull(scheduleDTO.getIsDeleted()) ? schedule.getIsDeleted() : scheduleDTO.getIsDeleted());

        Assert.isTrue(
                !repository.existsByClassIdAndDateAndSlot1AndSlot2AndSlot3AndSlot4AndSlot5AndSlot6AndSlot7AndSlot8AndIsDeleted(
                        schedule.getClassId(),
                        schedule.getDate(),
                        schedule.getSlot1(),
                        schedule.getSlot2(),
                        schedule.getSlot3(),
                        schedule.getSlot4(),
                        schedule.getSlot5(),
                        schedule.getSlot6(),
                        schedule.getSlot7(),
                        schedule.getSlot8(),
                        false),
                MessageUtils.getMessage(Constant.MessageCode.ERR_SLOT_IS_BOOKED));

        update(schedule);

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(() -> emailService.sendMailToClassMember(schedule.getClassId(), Constant.ScheduleStatus.UPDATE));
        return true;
    }

    @Override
    public Boolean deleteSchedule(Long scheduleId) {
        Schedule schedule = repository.findById(scheduleId).orElse(null);
        Assert.notNull(schedule, MessageUtils.getMessage(Constant.MessageCode.ERR_SCHEDULE_NOT_FOUND));

        schedule.setIsDeleted(true);
        update(schedule);

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(() -> emailService.sendMailToClassMember(schedule.getClassId(), Constant.ScheduleStatus.DELETE));

        return true;
    }

    @Override
    public List<CalendarDTO> getClassSchedule(Long classId, LocalDate fromDate, LocalDate toDate) {
        Assert.isTrue(classService.existById(classId), MessageUtils.getMessage(Constant.MessageCode.ERR_CLASS_NOT_FOUND));
        validateDate(fromDate, toDate);

        List<ScheduleDTO> scheduleDTOList = Transformer.transformToListScheduleDTO(repository.findAllByIsDeletedAndClassIdAndDateIsBetween(false, classId, fromDate, toDate));

        return mergeSchedule(scheduleDTOList, fromDate);
    }

    @Override
    public List<CalendarDTO> getStudentSchedule(Long studentId, LocalDate fromDate, LocalDate toDate) {
        Assert.isTrue(studentService.existById(studentId), MessageUtils.getMessage(Constant.MessageCode.ERR_STUDENT_NOT_FOUND));
        validateDate(fromDate, toDate);

        List<ClassStudentDTO> classStudentDTOList = classStudentService.findAllByStudentId(studentId);
        List<Long> classIdList = classStudentDTOList.stream().map(ClassStudentDTO::getClassId).collect(Collectors.toList());
        List<ScheduleDTO> scheduleDTOList = Transformer.transformToListScheduleDTO(repository.findAllByIsDeletedAndClassIdIsInAndDateIsBetween(false, classIdList, fromDate, toDate));

        return mergeSchedule(scheduleDTOList, fromDate);
    }

    @Override
    public List<CalendarDTO> getLecturerSchedule(Long lecturerId, LocalDate fromDate, LocalDate toDate) {
        Assert.isTrue(lecturerService.existById(lecturerId), MessageUtils.getMessage(Constant.MessageCode.ERR_LECTURER_NOT_FOUND));
        validateDate(fromDate, toDate);

        List<Long> classIdList = classService.findAllByLecturerId(lecturerId).stream().map(Class::getId).collect(Collectors.toList());
        List<ScheduleDTO> scheduleDTOList = Transformer.transformToListScheduleDTO(repository.findAllByIsDeletedAndClassIdIsInAndDateIsBetween(false, classIdList, fromDate, toDate));

        return mergeSchedule(scheduleDTOList, fromDate);
    }

    private void validateDate(LocalDate fromDate, LocalDate toDate) {
        Assert.isTrue(fromDate.plusDays(6L).equals(toDate), MessageUtils.getMessage(Constant.MessageCode.ERR_FROM_DATE_TO_DATE_MORE_THAN_SEVEN_DAY));

        DayOfWeek dayOfWeek = DayOfWeek.of(fromDate.get(ChronoField.DAY_OF_WEEK));
        Assert.isTrue(DayOfWeek.MONDAY.equals(dayOfWeek), MessageUtils.getMessage(Constant.MessageCode.ERR_FROM_DATE_IS_NOT_MONDAY));
    }

    private List<CalendarDTO> mergeSchedule(List<ScheduleDTO> scheduleDTOList, LocalDate fromDate) {
        scheduleDTOList.sort(Comparator.comparing(ScheduleDTO::getDate));

        List<CalendarDTO> result = new ArrayList<>(7);
        for (int i = 0; i < 7; i++) {
            LocalDate tmp = fromDate.plusDays(i);
            result.add(CalendarDTO.builder()
                    .date(tmp)
                    .slot1(SlotDTO.builder()
                            .classId(null)
                            .isBooked(false)
                            .build())
                    .slot2(SlotDTO.builder()
                            .classId(null)
                            .isBooked(false)
                            .build())
                    .slot3(SlotDTO.builder()
                            .classId(null)
                            .isBooked(false)
                            .build())
                    .slot4(SlotDTO.builder()
                            .classId(null)
                            .isBooked(false)
                            .build())
                    .slot5(SlotDTO.builder()
                            .classId(null)
                            .isBooked(false)
                            .build())
                    .slot6(SlotDTO.builder()
                            .classId(null)
                            .isBooked(false)
                            .build())
                    .slot7(SlotDTO.builder()
                            .classId(null)
                            .isBooked(false)
                            .build())
                    .slot8(SlotDTO.builder()
                            .classId(null)
                            .isBooked(false)
                            .build())
                    .build());
            for (ScheduleDTO scheduleDto : scheduleDTOList) {
                if (scheduleDto.getDate().plusDays(1L).equals(tmp)) {
                    result.set(i, DataUtils.mergeSchedule(result.get(i), scheduleDto));
                } else {
                    break;
                }
            }
            scheduleDTOList.removeIf(item -> item.getDate().plusDays(1L).equals(tmp));
        }
        return result;
    }
}
