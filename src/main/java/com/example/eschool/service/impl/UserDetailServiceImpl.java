package com.example.eschool.service.impl;

import com.example.eschool.common.Constant;
import com.example.eschool.common.Constant.MessageCode;
import com.example.eschool.model.entity.Account;
import com.example.eschool.repository.database.AccountRepository;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import static com.example.eschool.common.MessageUtils.getMessage;

@Service
public class UserDetailServiceImpl extends BaseServiceImpl<Account, Long, AccountRepository> implements UserDetailsService {

    public UserDetailServiceImpl(AccountRepository repository) {
        super(repository);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = repository.findByUsernameAndIsActiveAndIsDeleted(username, Constant.IsActive.TRUE, Constant.IsDeleted.FALSE);
        Assert.notNull(account, getMessage(MessageCode.ERR_WRONG_USERNAME_PASSWORD));
        return User.withUsername(account.getUsername()).password(account.getPassword()).roles(account.getRole()).build();
    }
}
