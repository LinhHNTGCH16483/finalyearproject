package com.example.eschool.service;

import com.example.eschool.model.entity.Menu;

public interface MenuService extends BaseService<Menu, Long> {
}
