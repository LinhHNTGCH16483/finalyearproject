package com.example.eschool.service;

import com.example.eschool.model.entity.Blog;

public interface BlogService extends BaseService<Blog, Long> {
}
