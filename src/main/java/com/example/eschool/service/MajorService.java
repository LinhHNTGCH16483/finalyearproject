package com.example.eschool.service;

import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Major;

public interface MajorService extends BaseService<Major, Long> {

    CountObjectResponseDTO countMajor();
}
