package com.example.eschool.service;

import com.example.eschool.model.dto.CalendarDTO;
import com.example.eschool.model.dto.ScheduleDTO;
import com.example.eschool.model.entity.Schedule;

import java.time.LocalDate;
import java.util.List;

public interface ScheduleService extends BaseService<Schedule, Long> {

    Boolean addSchedule(ScheduleDTO scheduleDTO);

    Boolean updateSchedule(Long scheduleId, ScheduleDTO scheduleDTO);

    Boolean deleteSchedule(Long scheduleId);

    List<CalendarDTO> getClassSchedule(Long classId, LocalDate fromDate, LocalDate toDate);

    List<CalendarDTO> getStudentSchedule(Long studentId, LocalDate fromDate, LocalDate toDate);

    List<CalendarDTO> getLecturerSchedule(Long lecturerId, LocalDate fromDate, LocalDate toDate);
}
