package com.example.eschool.service;

import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Course;

public interface CourseService extends BaseService<Course, Long> {

    CountObjectResponseDTO countCourse();
}
