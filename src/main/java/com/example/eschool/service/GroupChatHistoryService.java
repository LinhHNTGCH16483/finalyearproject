package com.example.eschool.service;

import com.example.eschool.model.dto.MessageDTO;
import com.example.eschool.model.entity.GroupChatHistory;

public interface GroupChatHistoryService extends BaseService<GroupChatHistory, Long> {

    void sendMessage(Long groupChatId, MessageDTO messageDTO);
}
