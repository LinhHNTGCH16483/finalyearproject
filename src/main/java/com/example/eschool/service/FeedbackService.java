package com.example.eschool.service;

import com.example.eschool.model.entity.Feedback;

public interface FeedbackService extends BaseService<Feedback, Long> {
}
