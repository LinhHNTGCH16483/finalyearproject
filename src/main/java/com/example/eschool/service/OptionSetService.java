package com.example.eschool.service;

import com.example.eschool.model.entity.OptionSet;

public interface OptionSetService extends BaseService<OptionSet, Long> {
}
