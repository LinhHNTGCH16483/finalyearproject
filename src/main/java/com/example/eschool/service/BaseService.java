package com.example.eschool.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Order;

import java.util.List;

public interface BaseService<Entity, ID> {

    boolean existById(ID id);

    Entity save(Entity entity);

    Entity getById(ID id);

    Entity update(Entity entity);

    void delete(ID id);

    List<Entity> getAll();

    Page<Entity> getPageWithOrders(Integer pageNumber, Integer pageSize, List<Order> orders);

    Page<Entity> getPageWithoutOrders(Integer pageNumber, Integer pageSize);
}
