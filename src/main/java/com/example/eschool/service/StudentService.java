package com.example.eschool.service;

import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.dto.SearchStudentResponseDTO;
import com.example.eschool.model.dto.StudentDTO;
import com.example.eschool.model.entity.Student;

public interface StudentService extends BaseService<Student, Long> {

    SearchStudentResponseDTO searchStudent(String studentCode, String studentName, Long pageSize, Long pageNumber);

    Boolean addStudent(StudentDTO studentDTO);

    Boolean updateStudent(Long studentId, StudentDTO studentDTO);

    Boolean deleteStudent(Long studentId);

    CountObjectResponseDTO countStudent();
}
