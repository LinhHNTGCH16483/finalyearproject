package com.example.eschool.service;

import com.example.eschool.model.redis.TokenRedis;

public interface RedisService {

    Boolean addToken(TokenRedis tokenRedis);

    Boolean updateToken(String username, String newToken);

    Boolean deleteToken(String username);

    TokenRedis findTokenByUsername(String username);
}
