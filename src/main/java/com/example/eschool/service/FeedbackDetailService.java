package com.example.eschool.service;

import com.example.eschool.model.entity.FeedbackDetail;

public interface FeedbackDetailService extends BaseService<FeedbackDetail, Long> {
}
