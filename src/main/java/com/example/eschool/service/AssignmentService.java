package com.example.eschool.service;

import com.example.eschool.model.entity.Assignment;

public interface AssignmentService extends BaseService<Assignment, Long> {
}
