package com.example.eschool.service;

import com.example.eschool.model.dto.ClassStudentDTO;
import com.example.eschool.model.dto.StudentDTO;
import com.example.eschool.model.entity.ClassStudent;

import java.util.List;

public interface ClassStudentService extends BaseService<ClassStudent, Long> {

    List<StudentDTO> getAllStudentByClassId(Long classId);

    List<ClassStudentDTO> findAllByStudentId(Long studentId);
}
