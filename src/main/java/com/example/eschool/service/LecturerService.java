package com.example.eschool.service;

import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Class;
import com.example.eschool.model.entity.Lecturer;

import java.util.Optional;

public interface LecturerService extends BaseService<Lecturer, Long> {

    Optional<Lecturer> findById(Long lecturerId);

    CountObjectResponseDTO countLecturer();
}
