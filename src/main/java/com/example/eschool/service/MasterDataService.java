package com.example.eschool.service;

import com.example.eschool.model.dto.MasterDataDTO;
import com.example.eschool.model.entity.MasterData;

import java.util.List;

public interface MasterDataService extends BaseService<MasterData, Long> {

    List<MasterDataDTO> getMasterData(String lang);
}
