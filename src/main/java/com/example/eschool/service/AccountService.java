package com.example.eschool.service;

import com.example.eschool.model.dto.AccountDTO;
import com.example.eschool.model.dto.LoginDTO;
import com.example.eschool.model.entity.Account;

import java.util.Optional;

public interface AccountService extends BaseService<Account, Long> {

    LoginDTO login(LoginDTO loginDTO);

    Boolean logout();

    AccountDTO findByUserName(String userName);

    Account saveAndFlush(Account account);

    Optional<Account> findById(Long accountId);

    Long countByRoleAndIsDeletedAndGender(String role, Boolean isDeleted, String gender);
}
