package com.example.eschool.service;

import com.example.eschool.model.dto.FileDTO;
import com.example.eschool.model.entity.File;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileService extends BaseService<File, Long> {

    FileDTO uploadFile(MultipartFile file) throws IOException;

    byte[] downloadFile(String bucketName, String filename) throws IOException;
}
