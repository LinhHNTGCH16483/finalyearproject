package com.example.eschool.service;

import com.example.eschool.common.Constant;

public interface EmailService {

    void sendMailToClassMember(Long classId, Constant.ScheduleStatus status);

    void sendEmail(String receiver, String content);
}
