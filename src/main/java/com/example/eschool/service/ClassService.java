package com.example.eschool.service;

import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Class;

import java.util.List;
import java.util.Optional;

public interface ClassService extends BaseService<Class, Long> {

    Optional<Class> findById(Long lecturerId);

    CountObjectResponseDTO countClass();

    Boolean existsById(Long classId);

    List<Class> findAllByLecturerId(Long lecturerId);
}
