package com.example.eschool.service;

import com.example.eschool.model.entity.GroupChatMember;

public interface GroupChatMemberService extends BaseService<GroupChatMember, Long> {

    boolean isMember(Long groupId, Long memberId);
}
