package com.example.eschool.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CountObjectResponseDTO implements Serializable {

    private static final long serialVersionUID = 8348079126969531172L;

    private Long total;

    private Long male;

    private Long female;

    private Long otherGender;
}
