package com.example.eschool.model.dto;

import com.example.eschool.config.datetime.LocalDateTimeDeserializer;
import com.example.eschool.config.datetime.LocalDateTimeSerializer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterDataDTO implements Serializable {

    private static final long serialVersionUID = 7295962744695618398L;

    private Long id;

    private String fieldId;

    private String lang;

    private String name;

    private Boolean isDeleted;

    private String createdBy;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime createdDate;

    private String lastModifiedBy;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime lastModifiedDate;

    public MasterDataDTO(Long id, String fieldId, String name) {
        this.id = id;
        this.fieldId = fieldId;
        this.name = name;
    }
}
