package com.example.eschool.model.dto;

import com.example.eschool.config.datetime.LocalDateDeserializer;
import com.example.eschool.config.datetime.LocalDateSerializer;
import com.example.eschool.config.datetime.LocalDateTimeDeserializer;
import com.example.eschool.config.datetime.LocalDateTimeSerializer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StudentDTO implements Serializable {

    private static final long serialVersionUID = -8232909075754721319L;

    private Long id;

    private Long accountId;

    private String code;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate admissionDate;

    private Boolean isDeleted;

    private String createdBy;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime createdDate;

    private String lastModifiedBy;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime lastModifiedDate;

    private String username;

    private String password;

    private String role;

    private Boolean isAccountActive;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime lastLogin;

    private String fullName;

    private String firstName;

    private String lastName;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate dateOfBirth;

    private String placeOfBirth;

    private String gender;

    private String nationality;

    private String religion;

    private String maritalStatus;

    private String permanentAddress;

    private String phoneNumber;

    private String email;

    private String avatarImage;

    private Boolean isAccountDeleted;

    private String accountCreatedBy;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime accountCreatedDate;

    private String accountLastModifiedBy;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime accountLastModifiedDate;
}
