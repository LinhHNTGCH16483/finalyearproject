package com.example.eschool.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CalendarDTO implements Serializable {

    private static final long serialVersionUID = 3261333170838799706L;

    private LocalDate date;
    private SlotDTO slot1;
    private SlotDTO slot2;
    private SlotDTO slot3;
    private SlotDTO slot4;
    private SlotDTO slot5;
    private SlotDTO slot6;
    private SlotDTO slot7;
    private SlotDTO slot8;
}
