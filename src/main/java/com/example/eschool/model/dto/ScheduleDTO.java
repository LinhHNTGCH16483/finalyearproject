package com.example.eschool.model.dto;

import com.example.eschool.config.datetime.LocalDateTimeDeserializer;
import com.example.eschool.config.datetime.LocalDateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScheduleDTO implements Serializable {

    private static final long serialVersionUID = -7731885141618745551L;

    private Long id;

    private Long classId;

    private LocalDate date;

    private Boolean slot1;

    private Boolean slot2;

    private Boolean slot3;

    private Boolean slot4;

    private Boolean slot5;

    private Boolean slot6;

    private Boolean slot7;

    private Boolean slot8;

    private Boolean isDeleted;

    private String createdBy;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime createdDate;

    private String lastModifiedBy;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime lastModifiedDate;
}
