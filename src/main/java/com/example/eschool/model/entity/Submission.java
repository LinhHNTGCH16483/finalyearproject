package com.example.eschool.model.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "submission")
public class Submission extends Audit implements Serializable {

    private static final long serialVersionUID = -1492680560234165994L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "assignment_id")
    private Long assignmentId;

    @Column(name = "class_student_id")
    private Long classStudentId;

    @Column(name = "file_id")
    private Long fileId;

    @Column(name = "mark")
    private Byte mark;

    @Column(name = "is_deleted")
    private Boolean isDelete;
}
