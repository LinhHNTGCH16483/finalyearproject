package com.example.eschool.model.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "assignment")
public class Assignment extends Audit implements Serializable {

    private static final long serialVersionUID = 4472067757087458903L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "class_id")
    private Long classId;

    @Column(name = "title")
    private String title;

    @Column(name = "content")
    private String content;

    @Column(name = "file_id")
    private String fileId;

    @Column(name = "deadline")
    private LocalDateTime deadline;

    @Column(name = "is_deleted")
    private Boolean isDeleted;
}
