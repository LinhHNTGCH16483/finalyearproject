package com.example.eschool.model.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "blog_comment")
public class BlogComment extends Audit implements Serializable {

    private static final long serialVersionUID = 7279740787160320874L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "blog_id")
    private Long blogId;

    @Column(name = "account_id")
    private Long accountId;

    @Column(name = "content")
    private String content;

    @Column(name = "is_deleted")
    private Boolean isDelete;
}
