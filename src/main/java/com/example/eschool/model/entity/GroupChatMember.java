package com.example.eschool.model.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "group_chat_member")
public class GroupChatMember extends Audit implements Serializable {

    private static final long serialVersionUID = -2322615597362737012L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "group_chat_id")
    private Long groupChatId;

    @Column(name = "account_id")
    private Long accountId;

    @Column(name = "is_deleted")
    private Boolean isDeleted;
}
