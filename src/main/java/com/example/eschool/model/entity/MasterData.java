package com.example.eschool.model.entity;

import com.example.eschool.common.Constant;
import com.example.eschool.model.dto.MasterDataDTO;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "master_data")
public class MasterData extends Audit implements Serializable {

    private static final long serialVersionUID = 2651769298545526965L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "field_id")
    private String fieldId;

    @Column(name = "lang")
    private String lang;

    @Column(name = "name")
    private String name;

    @Column(name = "is_deleted")
    private Boolean isDeleted;


}



