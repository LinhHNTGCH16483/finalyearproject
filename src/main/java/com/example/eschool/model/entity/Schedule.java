package com.example.eschool.model.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "schedule")
public class Schedule extends Audit implements Serializable {

    private static final long serialVersionUID = 2595019647777832421L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "class_id")
    private Long classId;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "slot_1")
    private Boolean slot1;

    @Column(name = "slot_2")
    private Boolean slot2;

    @Column(name = "slot_3")
    private Boolean slot3;

    @Column(name = "slot_4")
    private Boolean slot4;

    @Column(name = "slot_5")
    private Boolean slot5;

    @Column(name = "slot_6")
    private Boolean slot6;

    @Column(name = "slot_7")
    private Boolean slot7;

    @Column(name = "slot_8")
    private Boolean slot8;

    @Column(name = "is_deleted")
    private Boolean isDeleted;
}
