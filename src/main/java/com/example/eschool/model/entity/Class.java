package com.example.eschool.model.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "class")
public class Class extends Audit implements Serializable {

    private static final long serialVersionUID = 8041642850142641796L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "course_id")
    private Long courseId;

    @Column(name = "lecturer_id")
    private Long lecturerId;

    @Column(name = "code")
    private String code;

    @Column(name = "is_deleted")
    private Boolean isDeleted;
}
