package com.example.eschool.model.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "guardian")
public class Guardian extends Audit implements Serializable {

    private static final long serialVersionUID = -4360958812732855394L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "account_id")
    private Long accountId;

    @Column(name = "student_id")
    private Long studentId;

    @Column(name = "is_deleted")
    private Boolean isDeleted;
}
