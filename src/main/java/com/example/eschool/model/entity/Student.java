package com.example.eschool.model.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "student")
public class Student extends Audit implements Serializable {

    private static final long serialVersionUID = -6590204351984834214L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "account_id")
    private Long accountId;

    @Column(name = "code")
    private String code;

    @Column(name = "admission_date")
    private LocalDate admissionDate;

    @Column(name = "is_deleted")
    private Boolean isDeleted;
}
