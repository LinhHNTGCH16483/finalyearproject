package com.example.eschool.model.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "option_set")
public class OptionSet extends Audit implements Serializable {

    private static final long serialVersionUID = -2079257728888584619L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "parent_type")
    private String parentType;

    @Column(name = "type")
    private String type;

    @Column(name = "name")
    private String name;

    @Column(name = "is_deleted")
    private Boolean isDelete;
}
