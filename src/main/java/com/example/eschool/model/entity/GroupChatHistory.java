package com.example.eschool.model.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "group_chat_history")
public class GroupChatHistory extends Audit implements Serializable {

    private static final long serialVersionUID = 2758930187566037117L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "group_chat_id")
    private Long groupChatId;

    @Column(name = "group_chat_member_id")
    private Long groupChatMemberId;

    @Column(name = "content")
    private String content;

    @Column(name = "is_deleted")
    private Boolean isDelete;
}
