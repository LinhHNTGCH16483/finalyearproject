package com.example.eschool.model.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "feedback_detail")
public class FeedbackDetail extends Audit implements Serializable {

    private static final long serialVersionUID = -7980774082919329642L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "feedback_id")
    private Long feedbackId;

    @Column(name = "question")
    private String question;

    @Column(name = "rate")
    private Integer rate;

    @Column(name = "is_deleted")
    private Boolean isDelete;
}
