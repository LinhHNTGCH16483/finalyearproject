package com.example.eschool.model.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "menu")
public class Menu extends Audit implements Serializable {

    private static final long serialVersionUID = 4030820854150098869L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "parent_id")
    private Long parentId;

    @Column(name = "name")
    private String name;

    @Column(name = "router_link")
    private String routerLink;

    @Column(name = "role")
    private String role;

    @Column(name = "is_deleted")
    private Boolean isDelete;
}
