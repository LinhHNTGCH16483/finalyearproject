package com.example.eschool.model.redis;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RedisHash("test")
public class Test implements Serializable {

    private static final long serialVersionUID = 4758885388610650964L;

    @Id
    private String id;

    private String a;
    private String b;
    private String c;
}
