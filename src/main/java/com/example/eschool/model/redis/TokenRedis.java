package com.example.eschool.model.redis;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RedisHash("token")
public class TokenRedis implements Serializable {

    private static final long serialVersionUID = 9135389729539271875L;

    @Id
    private String id;

    private String username;
    private String token;
}
