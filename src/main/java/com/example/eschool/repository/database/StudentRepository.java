package com.example.eschool.repository.database;

import com.example.eschool.model.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends StudentRepositoryCustom, JpaRepository<Student, Long> {

    List<Student> findAllByIdIsInAndIsDeleted(List<Long> studentIdList, Boolean isDeleted);

    Long countByIsDeleted(Boolean isDeleted);
}
