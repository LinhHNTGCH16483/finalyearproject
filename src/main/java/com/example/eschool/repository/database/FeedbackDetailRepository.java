package com.example.eschool.repository.database;

import com.example.eschool.model.entity.FeedbackDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeedbackDetailRepository extends JpaRepository<FeedbackDetail, Long> {
}
