package com.example.eschool.repository.database;

import com.example.eschool.model.entity.Guardian;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GuardianRepository extends JpaRepository<Guardian, Long> {

    Long countByIsDeleted(Boolean isDeleted);
}
