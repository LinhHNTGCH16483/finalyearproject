package com.example.eschool.repository.database;

import com.example.eschool.model.entity.Lecturer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LecturerRepository extends JpaRepository<Lecturer, Long> {

    Long countByIsDeleted(Boolean isDeleted);
}
