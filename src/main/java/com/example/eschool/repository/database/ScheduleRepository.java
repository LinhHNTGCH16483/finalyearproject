package com.example.eschool.repository.database;

import com.example.eschool.model.entity.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Long> {

    Boolean existsByClassIdAndDateAndSlot1AndSlot2AndSlot3AndSlot4AndSlot5AndSlot6AndSlot7AndSlot8AndIsDeleted(Long classId,
                                                                                                               LocalDate date,
                                                                                                               Boolean slot1,
                                                                                                               Boolean slot2,
                                                                                                               Boolean slot3,
                                                                                                               Boolean slot4,
                                                                                                               Boolean slot5,
                                                                                                               Boolean slot6,
                                                                                                               Boolean slot7,
                                                                                                               Boolean slot8,
                                                                                                               Boolean isDeleted);

    List<Schedule> findAllByIsDeletedAndClassIdIsInAndDateIsBetween(Boolean isDeleted, List<Long> classIdList, LocalDate from, LocalDate to);

    List<Schedule> findAllByIsDeletedAndClassIdAndDateIsBetween(Boolean isDeleted, Long classIdList, LocalDate from, LocalDate to);
}
