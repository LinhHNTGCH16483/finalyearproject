package com.example.eschool.repository.database.impl;

import com.example.eschool.common.DataUtils;
import com.example.eschool.common.Transformer;
import com.example.eschool.model.dto.StudentDTO;
import com.example.eschool.repository.database.StudentRepositoryCustom;

import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StudentRepositoryCustomImpl extends BaseRepositoryCustomImpl implements StudentRepositoryCustom {
    @Override
    public List<StudentDTO> searchStudent(String studentCode, String studentName, Long pageSize, Long pageNumber) {
        Map<String, Object> params = new HashMap<>();

        String sql = buildQuery(studentCode, studentName, pageSize, pageNumber, false, params);

        Query query = entityManager.createNativeQuery(sql);

        params.forEach(query::setParameter);

        return Transformer.transformToListStudentDTO(query.getResultList());
    }

    @Override
    public Long countStudent(String studentCode, String studentName, Long pageSize, Long pageNumber) {
        Map<String, Object> params = new HashMap<>();

        String sql = buildQuery(studentCode, studentName, pageSize, pageNumber, true, params);

        Query query = entityManager.createNativeQuery(sql);

        params.forEach(query::setParameter);

        Object object = query.getSingleResult();
        return object != null ? Long.parseLong(object.toString()) : 0L;
    }

    private String buildQuery(String studentCode, String studentName, Long pageSize, Long pageNumber, boolean isCount, Map<String, Object> params) {
        StringBuilder stringQuery = new StringBuilder();
        if (isCount) {
            stringQuery.append("select count(*) ");
        } else {
            stringQuery.append("select s.id, ");
            stringQuery.append("       s.account_id, ");
            stringQuery.append("       s.code, ");
            stringQuery.append("       s.admission_date, ");
            stringQuery.append("       s.is_deleted as is_student_deleted, ");
            stringQuery.append("       s.created_by as student_created_by, ");
            stringQuery.append("       s.created_date as student_created_date, ");
            stringQuery.append("       s.last_modified_by as student_last_modified_by, ");
            stringQuery.append("       s.last_modified_date as student_last_modified_date, ");
            stringQuery.append("       a.username, ");
            stringQuery.append("       a.password, ");
            stringQuery.append("       a.role, ");
            stringQuery.append("       a.is_active, ");
            stringQuery.append("       a.last_login, ");
            stringQuery.append("       a.first_name, ");
            stringQuery.append("       a.last_name, ");
            stringQuery.append("       a.date_of_birth, ");
            stringQuery.append("       a.place_of_birth, ");
            stringQuery.append("       a.gender, ");
            stringQuery.append("       a.nationality, ");
            stringQuery.append("       a.religion, ");
            stringQuery.append("       a.marital_status, ");
            stringQuery.append("       a.permanent_address, ");
            stringQuery.append("       a.phone_number, ");
            stringQuery.append("       a.email, ");
            stringQuery.append("       a.avatar_image, ");
            stringQuery.append("       a.is_deleted as is_account_deleted, ");
            stringQuery.append("       a.created_by as account_created_by, ");
            stringQuery.append("       a.created_date as account_created_date, ");
            stringQuery.append("       a.last_modified_by as account_last_modified_by, ");
            stringQuery.append("       a.last_modified_date as account_last_modified_date ");
        }
        stringQuery.append("from student s ");
        stringQuery.append("         join account a on a.id = s.account_id ");
        stringQuery.append("where (a.is_deleted = 0 and s.is_deleted = 0) ");

        if (DataUtils.notNullOrEmpty(studentCode)) {
            stringQuery.append("  and s.code like CONCAT('%',:studentCode,'%') ");
            params.put("studentCode", studentCode);
        }
        if (DataUtils.notNullOrEmpty(studentName)) {
            stringQuery.append("  and a.full_name like CONCAT('%',:studentName,'%') ");
            params.put("studentName", studentName);
        }
        if (!isCount) {
            stringQuery.append("limit :limit offset :offset ");
            params.put("limit", pageSize);
            params.put("offset", (pageNumber - 1) * pageSize);
        }
        return stringQuery.toString();
    }
}
