package com.example.eschool.repository.database;

import com.example.eschool.model.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {

    Long countByIsDeleted(Boolean isDeleted);
}
