package com.example.eschool.repository.database;

import com.example.eschool.model.entity.GroupChatMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupChatMemberRepository extends JpaRepository<GroupChatMember, Long> {

    boolean existsByGroupChatIdAndAccountIdAndIsDeleted(Long groupChatId, Long accountId, Boolean isDeleted);
}
