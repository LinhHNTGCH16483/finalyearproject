package com.example.eschool.repository.database;

import com.example.eschool.model.entity.Major;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MajorRepository extends JpaRepository<Major, Long> {

    Long countByIsDeleted(boolean isDeleted);
}
