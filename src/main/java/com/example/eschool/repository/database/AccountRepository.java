package com.example.eschool.repository.database;

import com.example.eschool.model.entity.Account;
import com.example.eschool.model.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    Account findByUsernameAndIsActiveAndIsDeleted(String username, Boolean isActive, Boolean isDeleted);

    List<Account> findAllByIdIsInAndIsDeleted(List<Long> accountIdList, Boolean isDeleted);

    Long countByRoleAndIsDeletedAndGender(String role, Boolean isDeleted, String gender);
}
