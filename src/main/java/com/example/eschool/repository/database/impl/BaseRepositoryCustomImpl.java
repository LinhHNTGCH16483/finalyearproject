package com.example.eschool.repository.database.impl;

import com.example.eschool.common.Constant.ColumnName;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.Map;

@Slf4j
public abstract class BaseRepositoryCustomImpl {

    @PersistenceContext
    public EntityManager entityManager;

    protected <T> T getSingleResult(String query, String mapping, Map<String, Object> parameters) {
        Query nativeQuery = entityManager.createNativeQuery(query, mapping);
        if (CollectionUtils.isEmpty(parameters)) {
            return (T) nativeQuery.getSingleResult();
        }
        parameters.forEach(nativeQuery::setParameter);
        return (T) nativeQuery.getSingleResult();
    }

    protected <T> T getSingleResult(String query, Class<T> clazz, Map<String, Object> parameters) {
        Query nativeQuery = entityManager.createNativeQuery(query, clazz);
        if (CollectionUtils.isEmpty(parameters)) {
            return (T) nativeQuery.getSingleResult();
        }
        parameters.forEach(nativeQuery::setParameter);
        return (T) nativeQuery.getSingleResult();
    }

    protected <T> T getSingleResult(String query, Map<String, Object> parameters, Class<T> clazz) {
        Query nativeQuery = entityManager.createNativeQuery(query);
        if (CollectionUtils.isEmpty(parameters)) {
            return clazz.cast(nativeQuery.getSingleResult());
        }
        parameters.forEach(nativeQuery::setParameter);
        return clazz.cast(nativeQuery.getSingleResult());
    }

    protected <T> List<T> getResultList(String query, String mapping, Map<String, Object> parameters) {
        Query nativeQuery = entityManager.createNativeQuery(query, mapping);
        if (CollectionUtils.isEmpty(parameters)) {
            return nativeQuery.getResultList();
        }
        parameters.forEach(nativeQuery::setParameter);
        return nativeQuery.getResultList();
    }

    protected <T> List<T> getResultList(String query, Class<T> clazz, Map<String, Object> parameters) {
        Query nativeQuery = entityManager.createNativeQuery(query, clazz);
        if (CollectionUtils.isEmpty(parameters)) {
            return nativeQuery.getResultList();
        }
        parameters.forEach(nativeQuery::setParameter);
        return nativeQuery.getResultList();
    }

    protected List<Object[]> getResultList(String query, Map<String, Object> parameters) {
        Query nativeQuery = entityManager.createNativeQuery(query);
        if (CollectionUtils.isEmpty(parameters)) {
            return nativeQuery.getResultList();
        }
        parameters.forEach(nativeQuery::setParameter);
        return nativeQuery.getResultList();
    }

    protected int executeUpdate(String query, Map<String, Object> parameters) {
        Query nativeQuery = entityManager.createNativeQuery(query);
        if (CollectionUtils.isEmpty(parameters)) {
            return nativeQuery.executeUpdate();
        }
        parameters.forEach(nativeQuery::setParameter);
        return nativeQuery.executeUpdate();
    }

    protected int executeDelete(String tableName, Long id) {
        String sql = "UPDATE :tableName SET :isDeletedColumn = 1 WHERE :idColumn = :id";
        Query nativeQuery = entityManager.createNativeQuery(sql);
        nativeQuery.setParameter("tableName", tableName);
        nativeQuery.setParameter("isDeletedColumn", ColumnName.IS_DELETED);
        nativeQuery.setParameter("idColumn", ColumnName.ID);
        nativeQuery.setParameter("id", id);
        return nativeQuery.executeUpdate();
    }
}
