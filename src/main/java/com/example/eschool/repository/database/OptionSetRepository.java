package com.example.eschool.repository.database;

import com.example.eschool.model.entity.OptionSet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OptionSetRepository extends JpaRepository<OptionSet, Long> {
}
