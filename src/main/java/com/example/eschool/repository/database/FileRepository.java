package com.example.eschool.repository.database;

import com.example.eschool.model.entity.File;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepository extends JpaRepository<File, Long> {

    File findByBucketNameAndNameAndIsDelete(String bucketName, String fileName, boolean isDeleted);
}
