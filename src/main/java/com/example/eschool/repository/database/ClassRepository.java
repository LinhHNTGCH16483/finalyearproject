package com.example.eschool.repository.database;

import com.example.eschool.model.entity.Class;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClassRepository extends JpaRepository<Class, Long> {

    Long countByIsDeleted(Boolean isDeleted);

    List<Class> findAllByLecturerIdAndIsDeleted(Long lecturerId, Boolean isDeleted);
}
