package com.example.eschool.repository.database;

import com.example.eschool.model.dto.StudentDTO;

import java.util.List;

public interface StudentRepositoryCustom {

    List<StudentDTO> searchStudent(String studentCode, String studentName, Long pageSize, Long pageNumber);

    Long countStudent(String studentCode, String studentName, Long pageSize, Long pageNumber);
}
