package com.example.eschool.repository.database;

import com.example.eschool.model.entity.ClassStudent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClassStudentRepository extends JpaRepository<ClassStudent, Long> {

    List<ClassStudent> findAllByClassIdAndIsDeleted(Long classId, Boolean isDeleted);

    List<ClassStudent> findAllByStudentIdAndIsDeleted(Long studentId, Boolean isDeleted);
}
