package com.example.eschool.repository.database;

import com.example.eschool.model.entity.GroupChatHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupChatHistoryRepository extends JpaRepository<GroupChatHistory, Long> {
}
