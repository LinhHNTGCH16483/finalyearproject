package com.example.eschool.repository.redis;

import com.example.eschool.model.redis.Test;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestRepository extends CrudRepository<Test, String> {
}
