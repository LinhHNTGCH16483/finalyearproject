package com.example.eschool.repository.redis;

import com.example.eschool.model.redis.TokenRedis;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenRepository extends CrudRepository<TokenRedis, String> {

    TokenRedis findByUsername(String username);
}
