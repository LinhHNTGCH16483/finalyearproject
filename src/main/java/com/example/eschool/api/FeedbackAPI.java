package com.example.eschool.api;

import com.example.eschool.model.entity.Feedback;
import com.example.eschool.response.ResponseCustom;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "/api/v1/feedbacks")
public interface FeedbackAPI {

    @GetMapping()
    ResponseEntity<ResponseCustom<List<Feedback>>> getAllFeedback();

    @PostMapping()
    ResponseEntity<ResponseCustom<Boolean>> addFeedback(@RequestBody Feedback feedback);

    @PatchMapping()
    ResponseEntity<ResponseCustom<Boolean>> updateFeedback(@RequestParam(value = "feedbackId") Long feedbackId,
                                                           @RequestBody Feedback feedback);

    @DeleteMapping()
    ResponseEntity<ResponseCustom<Boolean>> deleteFeedback(@RequestParam(value = "feedbackId") Long feedbackId);
}
