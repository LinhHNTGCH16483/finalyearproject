package com.example.eschool.api;

import com.example.eschool.model.entity.Submission;
import com.example.eschool.response.ResponseCustom;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "/api/v1/submissions")
public interface SubmissionAPI {

    @GetMapping()
    ResponseEntity<ResponseCustom<List<Submission>>> getAllSubmission();

    @PostMapping()
    ResponseEntity<ResponseCustom<Boolean>> addSubmission(@RequestBody Submission submission);

    @PatchMapping()
    ResponseEntity<ResponseCustom<Boolean>> updateSubmission(@RequestParam(value = "submissionId") Long submissionId,
                                                             @RequestBody Submission submission);

    @DeleteMapping()
    ResponseEntity<ResponseCustom<Boolean>> deleteSubmission(@RequestParam(value = "submissionId") Long submissionId);
}
