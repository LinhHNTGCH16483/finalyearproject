package com.example.eschool.api;

import com.example.eschool.model.dto.MasterDataDTO;
import com.example.eschool.response.ResponseCustom;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@RequestMapping(value = "/api/v1/commons")
public interface CommonAPI {

    @GetMapping(value = "/master-data")
    ResponseEntity<ResponseCustom<List<MasterDataDTO>>> getMasterData(@RequestParam(value = "lang") String lang);
}
