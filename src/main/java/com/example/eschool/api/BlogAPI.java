package com.example.eschool.api;

import com.example.eschool.model.entity.Blog;
import com.example.eschool.response.ResponseCustom;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "/api/v1/blogs")
public interface BlogAPI {

    @GetMapping()
    ResponseEntity<ResponseCustom<List<Blog>>> getAllBlog();

    @PostMapping()
    ResponseEntity<ResponseCustom<Boolean>> addBlog(@RequestBody Blog blog);

    @PatchMapping()
    ResponseEntity<ResponseCustom<Boolean>> updateBlog(@RequestParam(value = "blogId") Long blogId,
                                                       @RequestBody Blog blog);

    @DeleteMapping()
    ResponseEntity<ResponseCustom<Boolean>> deleteBlog(@RequestParam(value = "blogId") Long blogId);
}
