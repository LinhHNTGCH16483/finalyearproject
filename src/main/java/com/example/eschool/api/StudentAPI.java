package com.example.eschool.api;

import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.dto.StudentDTO;
import com.example.eschool.response.ResponseCustom;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "/api/v1/students")
public interface StudentAPI {

    @GetMapping(value = "/search")
    ResponseEntity<ResponseCustom<List<StudentDTO>>> searchStudent(@RequestParam(value = "code", required = false) String studentCode,
                                                                   @RequestParam(value = "name", required = false) String studentName,
                                                                   @RequestParam(value = "pageSize", required = false, defaultValue = "10") Long pageSize,
                                                                   @RequestParam(value = "pageNumber", required = false, defaultValue = "1") Long pageNumber);

    @PostMapping()
    ResponseEntity<ResponseCustom<Boolean>> addStudent(@RequestBody StudentDTO studentDTO);

    @PatchMapping()
    ResponseEntity<ResponseCustom<Boolean>> updateStudent(@RequestParam(value = "studentId") Long studentId,
                                                             @RequestBody StudentDTO studentDTO);

    @DeleteMapping()
    ResponseEntity<ResponseCustom<Boolean>> deleteStudent(@RequestParam(value = "studentId") Long studentId);

    @GetMapping(value = "/count")
    ResponseEntity<ResponseCustom<CountObjectResponseDTO>> countStudent();
}
