package com.example.eschool.api;

import com.example.eschool.model.entity.Assignment;
import com.example.eschool.response.ResponseCustom;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "/api/v1/assignments")
public interface AssignmentAPI {

    @GetMapping()
    ResponseEntity<ResponseCustom<List<Assignment>>> getAllAssignment();

    @PostMapping()
    ResponseEntity<ResponseCustom<Boolean>> addAssignment(@RequestBody Assignment assignment);

    @PatchMapping()
    ResponseEntity<ResponseCustom<Boolean>> updateAssignment(@RequestParam(value = "assignmentId") Long assignmentId,
                                                             @RequestBody Assignment assignment);

    @DeleteMapping()
    ResponseEntity<ResponseCustom<Boolean>> deleteAssignment(@RequestParam(value = "assignmentId") Long assignmentId);
}
