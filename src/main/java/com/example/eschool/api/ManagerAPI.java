package com.example.eschool.api;

import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Manager;
import com.example.eschool.response.ResponseCustom;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "/api/v1/managers")
public interface ManagerAPI {

    @GetMapping(value = "/count")
    ResponseEntity<ResponseCustom<CountObjectResponseDTO>> countManager();

    @GetMapping()
    ResponseEntity<ResponseCustom<List<Manager>>> getAllManager();

    @PostMapping()
    ResponseEntity<ResponseCustom<Boolean>> addManager(@RequestBody Manager manager);

    @PatchMapping()
    ResponseEntity<ResponseCustom<Boolean>> updateManager(@RequestParam(value = "managerId") Long managerId,
                                                          @RequestBody Manager manager);

    @DeleteMapping()
    ResponseEntity<ResponseCustom<Boolean>> deleteManager(@RequestParam(value = "managerId") Long managerId);
}
