package com.example.eschool.api;

import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Guardian;
import com.example.eschool.response.ResponseCustom;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "/api/v1/guardians")
public interface GuardianAPI {

    @GetMapping(value = "/count")
    ResponseEntity<ResponseCustom<CountObjectResponseDTO>> countGuardian();

    @GetMapping()
    ResponseEntity<ResponseCustom<List<Guardian>>> getAllGuardian();

    @PostMapping()
    ResponseEntity<ResponseCustom<Boolean>> addGuardian(@RequestBody Guardian guardian);

    @PatchMapping()
    ResponseEntity<ResponseCustom<Boolean>> updateGuardian(@RequestParam(value = "guardianId") Long guardianId,
                                                           @RequestBody Guardian guardian);

    @DeleteMapping()
    ResponseEntity<ResponseCustom<Boolean>> deleteGuardian(@RequestParam(value = "guardianId") Long guardianId);
}
