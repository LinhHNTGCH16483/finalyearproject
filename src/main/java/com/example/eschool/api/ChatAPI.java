package com.example.eschool.api;

import com.example.eschool.model.dto.MessageDTO;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/api/v1/chat")
public interface ChatAPI {

    @MessageMapping("/{groupChatId}")
    void sendMessage(@DestinationVariable Long groupChatId, MessageDTO messageDTO);
}
