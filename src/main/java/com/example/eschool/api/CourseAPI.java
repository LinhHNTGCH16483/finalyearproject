package com.example.eschool.api;

import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Course;
import com.example.eschool.response.ResponseCustom;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "/api/v1/courses")
public interface CourseAPI {

    @GetMapping(value = "/count")
    ResponseEntity<ResponseCustom<CountObjectResponseDTO>> countCourse();

    @GetMapping()
    ResponseEntity<ResponseCustom<List<Course>>> getAllCourse();

    @PostMapping()
    ResponseEntity<ResponseCustom<Boolean>> addCourse(@RequestBody Course course);

    @PatchMapping()
    ResponseEntity<ResponseCustom<Boolean>> updateCourse(@RequestParam(value = "courseId") Long courseId,
                                                         @RequestBody Course course);

    @DeleteMapping()
    ResponseEntity<ResponseCustom<Boolean>> deleteCourse(@RequestParam(value = "courseId") Long courseId);
}
