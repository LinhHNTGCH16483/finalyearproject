package com.example.eschool.api;

import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Class;
import com.example.eschool.response.ResponseCustom;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "/api/v1/classes")
public interface ClassAPI {

    @GetMapping(value = "/count")
    ResponseEntity<ResponseCustom<CountObjectResponseDTO>> countClass();

    @GetMapping()
    ResponseEntity<ResponseCustom<List<Class>>> getAllClass();

    @PostMapping()
    ResponseEntity<ResponseCustom<Boolean>> addClass(@RequestBody Class clazz);

    @PatchMapping()
    ResponseEntity<ResponseCustom<Boolean>> updateClass(@RequestParam(value = "classId") Long classId,
                                                        @RequestBody Class clazz);

    @DeleteMapping()
    ResponseEntity<ResponseCustom<Boolean>> deleteClass(@RequestParam(value = "classId") Long classId);
}
