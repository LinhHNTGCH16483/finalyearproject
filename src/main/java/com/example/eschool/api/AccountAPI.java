package com.example.eschool.api;

import com.example.eschool.model.dto.LoginDTO;
import com.example.eschool.response.ResponseCustom;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/api/v1/accounts")
public interface AccountAPI {

    @PostMapping(value = "/log-in")
    ResponseEntity<ResponseCustom<LoginDTO>> login(@RequestBody LoginDTO loginDTO);

    @GetMapping(value = "/log-out")
    ResponseEntity<ResponseCustom<Boolean>> logout();
}
