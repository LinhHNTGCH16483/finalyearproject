package com.example.eschool.api;

import com.example.eschool.model.dto.FileDTO;
import com.example.eschool.response.ResponseCustom;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RequestMapping(value = "/api/v1/files")
public interface FileAPI {

    @PostMapping(value = "/upload")
    ResponseEntity<ResponseCustom<FileDTO>> uploadFile(@RequestParam(value = "file") MultipartFile file) throws IOException;

    @GetMapping(value = "/download")
    ResponseEntity<byte[]> downloadFile(@RequestParam(value = "bucketName") String bucketName,
                                        @RequestParam(value = "filename") String filename) throws IOException;
}
