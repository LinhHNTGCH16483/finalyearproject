package com.example.eschool.api;

import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Lecturer;
import com.example.eschool.response.ResponseCustom;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "/api/v1/lecturers")
public interface LecturerAPI {

    @GetMapping(value = "/count")
    ResponseEntity<ResponseCustom<CountObjectResponseDTO>> countLecturer();

    @GetMapping()
    ResponseEntity<ResponseCustom<List<Lecturer>>> getAllLecturer();

    @PostMapping()
    ResponseEntity<ResponseCustom<Boolean>> addLecturer(@RequestBody Lecturer lecturer);

    @PatchMapping()
    ResponseEntity<ResponseCustom<Boolean>> updateLecturer(@RequestParam(value = "lecturerId") Long lecturerId,
                                                           @RequestBody Lecturer lecturer);

    @DeleteMapping()
    ResponseEntity<ResponseCustom<Boolean>> deleteLecturer(@RequestParam(value = "lecturerId") Long lecturerId);
}
