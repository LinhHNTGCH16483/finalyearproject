package com.example.eschool.api;

import com.example.eschool.model.dto.CountObjectResponseDTO;
import com.example.eschool.model.entity.Major;
import com.example.eschool.response.ResponseCustom;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "/api/v1/majors")
public interface MajorAPI {

    @GetMapping(value = "/count")
    ResponseEntity<ResponseCustom<CountObjectResponseDTO>> countMajor();

    @GetMapping()
    ResponseEntity<ResponseCustom<List<Major>>> getAllMajor();

    @PostMapping()
    ResponseEntity<ResponseCustom<Boolean>> addMajor(@RequestBody Major major);

    @PatchMapping()
    ResponseEntity<ResponseCustom<Boolean>> updateMajor(@RequestParam(value = "majorId") Long majorId,
                                                        @RequestBody Major major);

    @DeleteMapping()
    ResponseEntity<ResponseCustom<Boolean>> deleteMajor(@RequestParam(value = "majorId") Long majorId);
}
