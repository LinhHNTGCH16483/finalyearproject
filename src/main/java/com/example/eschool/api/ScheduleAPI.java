package com.example.eschool.api;

import com.example.eschool.model.dto.CalendarDTO;
import com.example.eschool.model.dto.ScheduleDTO;
import com.example.eschool.response.ResponseCustom;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RequestMapping(value = "/api/v1/schedules")
public interface ScheduleAPI {

    @PostMapping()
    ResponseEntity<ResponseCustom<Boolean>> addSchedule(@RequestBody ScheduleDTO scheduleDTO);

    @PatchMapping()
    ResponseEntity<ResponseCustom<Boolean>> updateSchedule(@RequestParam(value = "scheduleId") Long scheduleId,
                                                           @RequestBody ScheduleDTO scheduleDTO);

    @DeleteMapping()
    ResponseEntity<ResponseCustom<Boolean>> deleteSchedule(@RequestParam(value = "scheduleId") Long scheduleId);

    @GetMapping(value = "/classes")
    ResponseEntity<ResponseCustom<List<CalendarDTO>>> getClassSchedule(@RequestParam("classId") Long classId,
                                                                       @RequestParam("fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
                                                                       @RequestParam("toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDate);

    @GetMapping(value = "/students")
    ResponseEntity<ResponseCustom<List<CalendarDTO>>> getStudentSchedule(@RequestParam("studentId") Long studentId,
                                                                         @RequestParam("fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
                                                                         @RequestParam("toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDate);

    @GetMapping(value = "lecturers")
    ResponseEntity<ResponseCustom<List<CalendarDTO>>> getLecturerSchedule(@RequestParam("lecturerId") Long lecturerId,
                                                                          @RequestParam("fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
                                                                          @RequestParam("toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDate);
}
