package com.example.eschool.api;

import com.example.eschool.model.dto.FileDTO;
import com.example.eschool.model.dto.LoginDTO;
import com.example.eschool.model.redis.Test;
import com.example.eschool.response.ResponseCustom;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RequestMapping("/test")
public interface TestAPI {

    @PostMapping(value = "/log-in")
    ResponseEntity<ResponseCustom<String>> login(@RequestBody LoginDTO loginDTO);

    @GetMapping(value = "/hello-world")
    ResponseEntity<ResponseCustom<String>> testHelloWorld(@RequestParam(value = "test", required = false) String test);

    @PostMapping(value = "/upload")
    ResponseEntity<ResponseCustom<FileDTO>> uploadFile(@RequestParam(value = "file") MultipartFile file) throws IOException;

    @GetMapping(value = "/download")
    ResponseEntity<byte[]> downloadFile(@RequestParam(value = "filename") String filename) throws IOException;

    @GetMapping(value = "/delete")
    ResponseEntity<ResponseCustom<Boolean>> deleteFile(@RequestParam(value = "filename") String filename);

    @GetMapping(value = "/write")
    ResponseEntity<ResponseCustom<Test>> write();

    @GetMapping(value = "/read")
    ResponseEntity<ResponseCustom<Test>> read();
}
