-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.12-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for e-school
DROP DATABASE IF EXISTS `e-school`;
CREATE DATABASE IF NOT EXISTS `e-school` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `e-school`;

-- Dumping structure for table e-school.account
DROP TABLE IF EXISTS `account`;
CREATE TABLE IF NOT EXISTS `account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(500) NOT NULL,
  `role` varchar(50) DEFAULT 'STUDENT',
  `is_active` tinyint(1) DEFAULT 1,
  `last_login` datetime DEFAULT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `place_of_birth` varchar(50) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `nationality` varchar(50) DEFAULT NULL,
  `religion` varchar(50) DEFAULT NULL,
  `marital_status` varchar(50) DEFAULT NULL,
  `permanent_address` varchar(50) DEFAULT NULL,
  `phone_number` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `avatar_image` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.account: ~6 rows (approximately)
DELETE FROM `account`;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` (`id`, `username`, `password`, `role`, `is_active`, `last_login`, `full_name`, `first_name`, `last_name`, `date_of_birth`, `place_of_birth`, `gender`, `nationality`, `religion`, `marital_status`, `permanent_address`, `phone_number`, `email`, `avatar_image`, `is_deleted`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES
	(1, 'admin', '$2a$10$iZDU0o/hYhc4ZuXU2cpyDOy7jv31gl7RxNX1Fp4DSOMgXlxp3p2lS', 'ADMIN', 1, '2020-06-29 21:41:07', 'Hoang Ngoc Tuan Linh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Linhhntgch16483@fpt.edu.vn', NULL, 0, NULL, NULL, NULL, NULL),
	(2, 'student', '$2a$10$iZDU0o/hYhc4ZuXU2cpyDOy7jv31gl7RxNX1Fp4DSOMgXlxp3p2lS', 'STUDENT', 1, '2020-06-29 21:41:07', 'STUDENT1', 'Tuan Linh', 'Hoang Ngoc', '1998-04-12', 'Ha Noi', 'Male', 'Vietnamese', NULL, 'Single', 'Ha Noi', '0346505008', 'Linhhntgch16483@fpt.edu.vn', NULL, 0, NULL, NULL, NULL, NULL),
	(3, 'guardian', '$2a$10$iZDU0o/hYhc4ZuXU2cpyDOy7jv31gl7RxNX1Fp4DSOMgXlxp3p2lS', 'GUARDIAN', 1, '2020-06-29 21:41:07', 'Hoang Ngoc Tuan Linh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Linhhntgch16483@fpt.edu.vn', NULL, 0, NULL, NULL, NULL, NULL),
	(4, 'lecturer', '$2a$10$iZDU0o/hYhc4ZuXU2cpyDOy7jv31gl7RxNX1Fp4DSOMgXlxp3p2lS', 'LECTURER', 1, '2020-06-29 21:41:07', 'LECTURER1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Linhhntgch16483@fpt.edu.vn', NULL, 0, NULL, NULL, NULL, NULL),
	(5, 'manager', '$2a$10$iZDU0o/hYhc4ZuXU2cpyDOy7jv31gl7RxNX1Fp4DSOMgXlxp3p2lS', 'MANAGER', 1, '2020-06-29 21:41:07', 'Hoang Ngoc Tuan Linh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Linhhntgch16483@fpt.edu.vn', NULL, 0, NULL, NULL, NULL, NULL),
	(6, 'test', '$2a$10$3Lv6eyYSUN5REMSkCo5DO.fN9RzfsYdaykc5Qmo98M.wz0EZkoBEa', 'STUDENT', 1, '2020-12-05 16:55:14', 'STUDENT2', 'A', 'Nguyen Van', '1998-04-11', 'Ha Noi', 'Male', 'Vietnamese', 'Viet Nam', 'Single', 'Ha Noi', '0346505008', 'Linhhntgch16483@fpt.edu.vn', 'abc', 0, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;

-- Dumping structure for table e-school.admin
DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_id` bigint(20) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__account_admin` (`account_id`),
  CONSTRAINT `FK__account_admin` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.admin: ~0 rows (approximately)
DELETE FROM `admin`;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;

-- Dumping structure for table e-school.assignment
DROP TABLE IF EXISTS `assignment`;
CREATE TABLE IF NOT EXISTS `assignment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `class_id` bigint(20) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `content` varchar(50) DEFAULT NULL,
  `file_id` bigint(20) DEFAULT NULL,
  `deadline` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__class_assignment` (`class_id`),
  KEY `FK_file_assignment` (`file_id`),
  CONSTRAINT `FK__class_assignment` FOREIGN KEY (`class_id`) REFERENCES `class` (`id`),
  CONSTRAINT `FK_file_assignment` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.assignment: ~0 rows (approximately)
DELETE FROM `assignment`;
/*!40000 ALTER TABLE `assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `assignment` ENABLE KEYS */;

-- Dumping structure for table e-school.blog
DROP TABLE IF EXISTS `blog`;
CREATE TABLE IF NOT EXISTS `blog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_id` bigint(20) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `content` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_account_blog` (`account_id`),
  CONSTRAINT `FK_account_blog` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.blog: ~0 rows (approximately)
DELETE FROM `blog`;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;

-- Dumping structure for table e-school.blog_comment
DROP TABLE IF EXISTS `blog_comment`;
CREATE TABLE IF NOT EXISTS `blog_comment` (
  `id` bigint(20) NOT NULL,
  `blog_id` bigint(20) DEFAULT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  `content` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__blog_blog_comment` (`blog_id`),
  KEY `FK_account_blog_comment` (`account_id`),
  CONSTRAINT `FK__blog_blog_comment` FOREIGN KEY (`blog_id`) REFERENCES `blog` (`id`),
  CONSTRAINT `FK_account_blog_comment` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.blog_comment: ~0 rows (approximately)
DELETE FROM `blog_comment`;
/*!40000 ALTER TABLE `blog_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog_comment` ENABLE KEYS */;

-- Dumping structure for table e-school.class
DROP TABLE IF EXISTS `class`;
CREATE TABLE IF NOT EXISTS `class` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `course_id` bigint(20) NOT NULL,
  `lecturer_id` bigint(20) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_course_class` (`course_id`),
  KEY `FK_lecturer_class` (`lecturer_id`),
  CONSTRAINT `FK_course_class` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  CONSTRAINT `FK_lecturer_class` FOREIGN KEY (`lecturer_id`) REFERENCES `lecturer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.class: ~1 rows (approximately)
DELETE FROM `class`;
/*!40000 ALTER TABLE `class` DISABLE KEYS */;
INSERT INTO `class` (`id`, `course_id`, `lecturer_id`, `code`, `is_deleted`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES
	(1, 1, 1, 'CLA433553989717600', 0, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `class` ENABLE KEYS */;

-- Dumping structure for table e-school.class_student
DROP TABLE IF EXISTS `class_student`;
CREATE TABLE IF NOT EXISTS `class_student` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `class_id` bigint(20) DEFAULT NULL,
  `student_id` bigint(20) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_class_class_member` (`class_id`),
  KEY `FK_student_class_member` (`student_id`),
  CONSTRAINT `FK_class_class_member` FOREIGN KEY (`class_id`) REFERENCES `class` (`id`),
  CONSTRAINT `FK_student_class_member` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.class_student: ~2 rows (approximately)
DELETE FROM `class_student`;
/*!40000 ALTER TABLE `class_student` DISABLE KEYS */;
INSERT INTO `class_student` (`id`, `class_id`, `student_id`, `is_deleted`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES
	(1, 1, 1, 0, NULL, NULL, NULL, NULL),
	(2, 1, 2, 0, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `class_student` ENABLE KEYS */;

-- Dumping structure for table e-school.course
DROP TABLE IF EXISTS `course`;
CREATE TABLE IF NOT EXISTS `course` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `major_id` bigint(20) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__major_course` (`major_id`),
  CONSTRAINT `FK__major_course` FOREIGN KEY (`major_id`) REFERENCES `major` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.course: ~1 rows (approximately)
DELETE FROM `course`;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` (`id`, `major_id`, `code`, `name`, `description`, `is_deleted`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES
	(1, 1, 'COU433553989701400', 'Object Oriented Programming', NULL, 0, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;

-- Dumping structure for table e-school.feedback
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE IF NOT EXISTS `feedback` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `class_id` bigint(20) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__class_feedback` (`class_id`),
  CONSTRAINT `FK__class_feedback` FOREIGN KEY (`class_id`) REFERENCES `class` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.feedback: ~0 rows (approximately)
DELETE FROM `feedback`;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;

-- Dumping structure for table e-school.feedback_detail
DROP TABLE IF EXISTS `feedback_detail`;
CREATE TABLE IF NOT EXISTS `feedback_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `feedback_id` bigint(20) DEFAULT NULL,
  `question` varchar(50) DEFAULT NULL,
  `rate` tinyint(1) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__feedback_feedback_detail` (`feedback_id`),
  CONSTRAINT `FK__feedback_feedback_detail` FOREIGN KEY (`feedback_id`) REFERENCES `feedback` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.feedback_detail: ~0 rows (approximately)
DELETE FROM `feedback_detail`;
/*!40000 ALTER TABLE `feedback_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback_detail` ENABLE KEYS */;

-- Dumping structure for table e-school.file
DROP TABLE IF EXISTS `file`;
CREATE TABLE IF NOT EXISTS `file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bucket_name` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `size` bigint(20) DEFAULT NULL,
  `extension` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.file: ~10 rows (approximately)
DELETE FROM `file`;
/*!40000 ALTER TABLE `file` DISABLE KEYS */;
INSERT INTO `file` (`id`, `bucket_name`, `name`, `type`, `size`, `extension`, `is_deleted`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES
	(1, NULL, '20201203_1607014392650_Danh_sach.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 8988, 'xlsx', NULL, NULL, NULL, NULL, NULL),
	(2, NULL, '20201203_1607014701403_Danh_sach.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 8988, 'xlsx', NULL, NULL, NULL, NULL, NULL),
	(3, NULL, '20201203_1607014728513_Danh_sach.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 8988, 'xlsx', NULL, NULL, NULL, NULL, NULL),
	(4, NULL, '20201203_1607014807282_Danh_sach.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 8988, 'xlsx', NULL, NULL, NULL, NULL, NULL),
	(5, NULL, '20201203_1607014940958_Danh_sach.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 8988, 'xlsx', NULL, NULL, NULL, NULL, NULL),
	(6, 'admin', '20201203_1607015065705_Danh_sach.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 8988, 'xlsx', NULL, NULL, NULL, NULL, NULL),
	(7, 'admin', '20201203_1607015097808_Danh_sach.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 8988, 'xlsx', NULL, NULL, NULL, NULL, NULL),
	(8, 'admin', '20201203_1607015199493_Danh_sach.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 8988, 'xlsx', 0, NULL, NULL, NULL, NULL),
	(9, 'admin', '20201203_1607015520143_Danh_sach.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 8988, 'xlsx', 0, NULL, NULL, NULL, NULL),
	(10, 'admin', '20201203_1607015596571_Danh_sach.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 8988, 'xlsx', 0, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `file` ENABLE KEYS */;

-- Dumping structure for table e-school.group_chat
DROP TABLE IF EXISTS `group_chat`;
CREATE TABLE IF NOT EXISTS `group_chat` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.group_chat: ~0 rows (approximately)
DELETE FROM `group_chat`;
/*!40000 ALTER TABLE `group_chat` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_chat` ENABLE KEYS */;

-- Dumping structure for table e-school.group_chat_history
DROP TABLE IF EXISTS `group_chat_history`;
CREATE TABLE IF NOT EXISTS `group_chat_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_chat_id` bigint(20) DEFAULT NULL,
  `group_chat_member_id` bigint(20) DEFAULT NULL,
  `content` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__group_chat_group_chat_history` (`group_chat_id`),
  KEY `FK_group_chat_history_group_chat_member` (`group_chat_member_id`),
  CONSTRAINT `FK__group_chat_group_chat_history` FOREIGN KEY (`group_chat_id`) REFERENCES `group_chat` (`id`),
  CONSTRAINT `FK_group_chat_history_group_chat_member` FOREIGN KEY (`group_chat_member_id`) REFERENCES `group_chat_member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.group_chat_history: ~0 rows (approximately)
DELETE FROM `group_chat_history`;
/*!40000 ALTER TABLE `group_chat_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_chat_history` ENABLE KEYS */;

-- Dumping structure for table e-school.group_chat_member
DROP TABLE IF EXISTS `group_chat_member`;
CREATE TABLE IF NOT EXISTS `group_chat_member` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_chat_id` bigint(20) DEFAULT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__group_chat_group_chat_member` (`group_chat_id`),
  KEY `FK_group_chat_member_account` (`account_id`),
  CONSTRAINT `FK__group_chat_group_chat_member` FOREIGN KEY (`group_chat_id`) REFERENCES `group_chat` (`id`),
  CONSTRAINT `FK_group_chat_member_account` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.group_chat_member: ~0 rows (approximately)
DELETE FROM `group_chat_member`;
/*!40000 ALTER TABLE `group_chat_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_chat_member` ENABLE KEYS */;

-- Dumping structure for table e-school.guardian
DROP TABLE IF EXISTS `guardian`;
CREATE TABLE IF NOT EXISTS `guardian` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_id` bigint(20) DEFAULT NULL,
  `student_id` bigint(20) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__account_guardian` (`account_id`),
  KEY `FK__student_guardian` (`student_id`),
  CONSTRAINT `FK__account_guardian` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK__student_guardian` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.guardian: ~0 rows (approximately)
DELETE FROM `guardian`;
/*!40000 ALTER TABLE `guardian` DISABLE KEYS */;
/*!40000 ALTER TABLE `guardian` ENABLE KEYS */;

-- Dumping structure for table e-school.lecturer
DROP TABLE IF EXISTS `lecturer`;
CREATE TABLE IF NOT EXISTS `lecturer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_id` bigint(20) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__account_lecturer` (`account_id`),
  CONSTRAINT `FK__account_lecturer` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.lecturer: ~1 rows (approximately)
DELETE FROM `lecturer`;
/*!40000 ALTER TABLE `lecturer` DISABLE KEYS */;
INSERT INTO `lecturer` (`id`, `account_id`, `is_deleted`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES
	(1, 4, 0, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `lecturer` ENABLE KEYS */;

-- Dumping structure for table e-school.major
DROP TABLE IF EXISTS `major`;
CREATE TABLE IF NOT EXISTS `major` (
  `id` bigint(20) NOT NULL DEFAULT 0,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.major: ~1 rows (approximately)
DELETE FROM `major`;
/*!40000 ALTER TABLE `major` DISABLE KEYS */;
INSERT INTO `major` (`id`, `code`, `name`, `description`, `is_deleted`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES
	(1, 'MAJ433553989670400', 'Information Technology', NULL, 0, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `major` ENABLE KEYS */;

-- Dumping structure for table e-school.manager
DROP TABLE IF EXISTS `manager`;
CREATE TABLE IF NOT EXISTS `manager` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_id` bigint(20) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__account_manager` (`account_id`),
  CONSTRAINT `FK__account_manager` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.manager: ~0 rows (approximately)
DELETE FROM `manager`;
/*!40000 ALTER TABLE `manager` DISABLE KEYS */;
/*!40000 ALTER TABLE `manager` ENABLE KEYS */;

-- Dumping structure for table e-school.master_data
DROP TABLE IF EXISTS `master_data`;
CREATE TABLE IF NOT EXISTS `master_data` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `field_id` varchar(50) DEFAULT NULL,
  `lang` varchar(50) DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.master_data: ~2 rows (approximately)
DELETE FROM `master_data`;
/*!40000 ALTER TABLE `master_data` DISABLE KEYS */;
INSERT INTO `master_data` (`id`, `field_id`, `lang`, `name`, `is_deleted`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES
	(1, 'text_login', 'vi', 'Đăng Nhập', 0, 'LinhHNT', '2020-08-17 14:04:25', NULL, NULL),
	(2, 'text_login', 'en', 'Login', 0, 'LinhHNT', '2020-08-17 14:04:25', NULL, NULL);
/*!40000 ALTER TABLE `master_data` ENABLE KEYS */;

-- Dumping structure for table e-school.menu
DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `router_link` varchar(50) DEFAULT NULL,
  `role` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_menu_menu` (`parent_id`),
  CONSTRAINT `FK_menu_menu` FOREIGN KEY (`parent_id`) REFERENCES `menu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.menu: ~0 rows (approximately)
DELETE FROM `menu`;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;

-- Dumping structure for table e-school.option_set
DROP TABLE IF EXISTS `option_set`;
CREATE TABLE IF NOT EXISTS `option_set` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_type` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.option_set: ~4 rows (approximately)
DELETE FROM `option_set`;
/*!40000 ALTER TABLE `option_set` DISABLE KEYS */;
INSERT INTO `option_set` (`id`, `parent_type`, `type`, `name`, `is_deleted`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES
	(1, '', 'gender', 'Gender', 0, NULL, NULL, NULL, NULL),
	(2, 'gender', 'male', 'Male', 0, NULL, NULL, NULL, NULL),
	(3, 'gender', 'female', 'Female', 0, NULL, NULL, NULL, NULL),
	(4, 'gender', 'other', 'Other', 0, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `option_set` ENABLE KEYS */;

-- Dumping structure for table e-school.schedule
DROP TABLE IF EXISTS `schedule`;
CREATE TABLE IF NOT EXISTS `schedule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `class_id` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `slot_1` tinyint(1) DEFAULT 0,
  `slot_2` tinyint(1) DEFAULT 0,
  `slot_3` tinyint(1) DEFAULT 0,
  `slot_4` tinyint(1) DEFAULT 0,
  `slot_5` tinyint(1) DEFAULT 0,
  `slot_6` tinyint(1) DEFAULT 0,
  `slot_7` tinyint(1) DEFAULT 0,
  `slot_8` tinyint(1) DEFAULT 0,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_schedule_detail_class` (`class_id`),
  CONSTRAINT `FK_schedule_detail_class` FOREIGN KEY (`class_id`) REFERENCES `class` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.schedule: ~6 rows (approximately)
DELETE FROM `schedule`;
/*!40000 ALTER TABLE `schedule` DISABLE KEYS */;
INSERT INTO `schedule` (`id`, `class_id`, `date`, `slot_1`, `slot_2`, `slot_3`, `slot_4`, `slot_5`, `slot_6`, `slot_7`, `slot_8`, `is_deleted`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES
	(1, 1, '2020-11-29', 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
	(2, 1, '2020-11-30', 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
	(3, 1, '2020-11-30', 0, 1, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
	(4, 1, '2020-12-01', 0, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
	(5, 1, '2020-12-01', 0, 0, 0, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
	(6, 1, '2020-12-06', 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
	(7, 1, '2020-12-07', 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `schedule` ENABLE KEYS */;

-- Dumping structure for table e-school.student
DROP TABLE IF EXISTS `student`;
CREATE TABLE IF NOT EXISTS `student` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_id` bigint(20) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `admission_date` date DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__account_student` (`account_id`),
  CONSTRAINT `FK__account_student` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.student: ~2 rows (approximately)
DELETE FROM `student`;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` (`id`, `account_id`, `code`, `admission_date`, `is_deleted`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES
	(1, 2, 'STD16001', '2016-09-01', 0, NULL, NULL, NULL, NULL),
	(2, 6, 'STU2020407672060859200', '2020-08-31', 0, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;

-- Dumping structure for table e-school.submission
DROP TABLE IF EXISTS `submission`;
CREATE TABLE IF NOT EXISTS `submission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assignment_id` bigint(20) DEFAULT NULL,
  `class_student_id` bigint(20) DEFAULT NULL,
  `file_id` bigint(20) DEFAULT NULL,
  `mark` tinyint(3) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_submission_assignment` (`assignment_id`),
  KEY `FK_submission_file` (`file_id`),
  KEY `FK__class_member_submission` (`class_student_id`),
  CONSTRAINT `FK__class_member_submission` FOREIGN KEY (`class_student_id`) REFERENCES `class_student` (`id`),
  CONSTRAINT `FK_submission_assignment` FOREIGN KEY (`assignment_id`) REFERENCES `assignment` (`id`),
  CONSTRAINT `FK_submission_file` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table e-school.submission: ~0 rows (approximately)
DELETE FROM `submission`;
/*!40000 ALTER TABLE `submission` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
