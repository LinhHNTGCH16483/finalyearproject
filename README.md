1. Install MinIO Server:
   1.1 Link download: https://dl.min.io/server/minio/release/windows-amd64/minio.exe
   1.2 Open command prompt in the folder contain file minio.exe just downloaded
   1.3 Run command:
        set MINIO_ACCESS_KEY=root
        set MINIO_SECRET_KEY=Linhhntgch16483
        minio server `folder_store_data` --address `localhost`:`9000`
2. Setting JDK and MAVEN:
    2.1 JDK 11:
        2.1.1: Link download: https://www.oracle.com/java/technologies/javase-jdk11-downloads.html
        2.1.2: Install the JDK 11
    2.2 MAVEN:
        2.2.1: Link download: https://maven.apache.org/download.cgi
        2.2.2: Unzip file: apache-maven-3.6.3-bin.zip
    2.3 Setting environment:
        2.3.1: Link: Control Panel -> All Control Panel Items -> System -> Advanced system settings -> Environment Variables
        2.3.2: Add Variables:
            JAVA_HOME: `JDK 11 folder`
            M2_HOME: `MAVEN folder`
            Path:   %JAVA_HOME%\bin
                    %M2_HOME%\bin
3. Install and setting IDE: Intellij
    3.1: Link download: https://www.jetbrains.com/idea/download/#section=windows
    3.2: Install file ideaIU-2019.3.4.exe
    3.3: Setting IDE:
        3.3.1: Run Intellij then choose: Configure -> Settings
        3.3.2: Search for "maven"
            Change the "Maven home dictionary" to the maven folder
            Change the "User setting file" to the path : *\apache-maven-3.6.3\conf\settings.xml
        3.3.3: Search for "plugins"
            Choose the "Market Place"
            Search for "lombok"
            Install lombok and restart IDE
4. Clone the project:
    4.1: Link repository: https://LinhHNTGCH16483@bitbucket.org/LinhHNTGCH16483/finalyearproject.git
    4.2: Clone the project
    4.3: Open Intellij -> choose "Open or Import" -> choose the cloned repository -> choose "Maven"
    